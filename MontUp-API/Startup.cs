﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Model;
using Model.Identity;
using MontUp_API.Dto;
using MontUp_API.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Repository.Database;
using Repository.Interface;
using Repository.Repositories;

namespace MontUp_API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IdentityBuilder builder = services.AddIdentityCore<Usuario>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 4;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(Role), builder.Services);
            builder.AddEntityFrameworkStores<SistemaContext>();
            builder.AddRoleValidator<RoleManager<Role>>();
            builder.AddRoleManager<RoleManager<Role>>();
            builder.AddSignInManager<SignInManager<Usuario>>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII
                                .GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    }
                );


            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllHeaders",
                    x=>
                    {
                        x.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowCredentials();
                    });
            });


            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            })
             .AddJsonOptions(options =>
             {

                 options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                 options.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Ignore;
                 options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                 options.SerializerSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
             })
              .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.CreateMap<Usuario, UsuarioDto>().ReverseMap();
                mc.CreateMap<Usuario, UsuarioLoginDto>().ReverseMap();
            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddScoped(typeof(IBaseReadRepository), typeof(GenericRepository));
            services.AddScoped(typeof(IBaseRepository), typeof(GenericRepository));
            services.AddScoped(typeof(IImagemMontanhaRepository), typeof(ImagemMontanhaRepository));
            services.AddScoped(typeof(IMensagemRepository), typeof(MensagemRepository));
            services.AddScoped(typeof(IMontanhaCategoriaRepository), typeof(MontanhaCategoriaRepository));
            services.AddScoped(typeof(ILocalRepository), typeof(LocalRepository));
            services.AddScoped(typeof(IComentarioRepository), typeof(ComentarioRepository));
            services.AddScoped(typeof(IChatRepository), typeof(ChatRepository));
            services.AddScoped(typeof(IGuiaRepository), typeof(GuiaRepository));
            services.AddScoped(typeof(IMontanhaRepository), typeof(MontanhaRepository));
            services.AddScoped(typeof(IUsuarioRepository), typeof(UsuarioRepository));

            // SqlServer
            services.AddDbContext<SistemaContext>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            // MySQL
            //services.AddDbContext<SistemaContext>(x => x.UseMySql(Configuration.GetConnectionString("MySQL")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();

            app.UseCors("AllowAllHeaders");

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
