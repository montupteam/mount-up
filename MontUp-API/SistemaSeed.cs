﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
    public static class SistemaSeed
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            #region usuarios
            modelBuilder.Entity<Usuario>().HasData(
                new Usuario()
                {
                    Id = 1,
                    UserName = "Andre",
                    Nome = "Andre",
                    Sobrenome = "Hinckel",
                    Email = "andre@gmail.com",
                    NormalizedUserName = "ANDRE",
                    Password = "e1223d9bbcd82236f9f09ae1f5578e3cbbd4e8f48954cead3003be60ac85629726dc04b1f875353459f97ba4a4283a1a6adb89d3524bb4816c7125964097106c", //1234
                    NormalizedEmail = "ANDRE@GMAIL.COM",
                    PasswordHash = "AQAAAAEAACcQAAAAEMCnl8w9nsLU3srK79HqtaA3mOq4ZKyJrfcLqV1370URklz5P9xTJ9LjG0dh5nYDcg==",
                    SecurityStamp = "PZ2DX2TZJUKNQUVLSJEUKSV6CLEO6K2K",
                    ConcurrencyStamp = "15b4e749-47f0-449e-a194-27d7c7fcdb62",
                    LockoutEnabled = true,
                    DataCriacao = DateTime.Now,
                    RegistroAtivo = true
                },
                new Usuario()
                {
                    Id = 2,
                    UserName = "Gustavo",
                    Nome = "Gustavo",
                    Sobrenome = "Manoel",
                    Email = "Gustavo@gmail.com",
                    NormalizedUserName = "GUSTAVO",
                    Password = "dc199c20e841df931a283634e6d8fa19ef7ef4988acd55b7cb0b938b8e334c8b073b6e2a597d4930f75b028960655df5852b6b1601f56bfe5eb31beccf06788d", //12345
                    NormalizedEmail = "GUSTAVO@GMAIL.COM",
                    PasswordHash = "AQAAAAEAACcQAAAAEEhdQqr/qBuj0o0mbBaf/Kdol63nbfWJOzKO7mxra50WzWcvIetByNeR5dS7VyhmJQ==",
                    SecurityStamp = "ZH3JKPCMZQD3KRA3TJU5FPR7BUBXBZKT",
                    ConcurrencyStamp = "47c8fde7-f39b-4677-b97a-b96b05a92e50",
                    LockoutEnabled = true,
                    DataCriacao = DateTime.Now,
                    RegistroAtivo = true
                },
                new Usuario()
                {
                    Id = 3,
                    UserName = "Nathan",
                    Nome = "Nathan",
                    Sobrenome = "Micael",
                    Email = "nathanalves001@gmail.com",
                    NormalizedUserName = "NATHAN",
                    Password = "0a47151a074e633ab7b6bed6aab724abbddcd3250f80a06bc612a233a907805101f2441b5b2926e54ce8ac8cfbc074bb7a56748830487df09591dbe167e800f6", //123456
                    NormalizedEmail = "NATHANALVES001@GMAIL.COM",
                    PasswordHash = "AQAAAAEAACcQAAAAEDasw8SklpHTWK1/2tAU/kXENWbNGtFRCSNX6+dilI0lfXiKQMXNlNetPnx1uVTr5g==",
                    SecurityStamp = "RKYYTWOWCTGQXECW4FCHGLOAAOTWE6RA",
                    ConcurrencyStamp = "371e6bf6-2787-4135-a518-ec5e9ab394e6",
                    LockoutEnabled = true,
                    DataCriacao = DateTime.Now,
                    RegistroAtivo = true
                },
                new Usuario()
                {
                    Id = 4,
                    UserName = "Eduardo",
                    Nome = "Eduardo",
                    Sobrenome = "Gabriel",
                    Email = "eduardo@gmail.com",
                    NormalizedUserName = "EDUARDO",
                    Password = "bdb36a0a12e22a77ffe7ab04d03c6fe209c8dce452eab3d58733b3f5e7133baf92202a9b78619fdd5e24db0ad11fde37210be53782d33c8072fbc907c5ade8ff", //1234567
                    NormalizedEmail = "EDUARDO@GMAIL.COM",
                    PasswordHash = "AQAAAAEAACcQAAAAECcvAzTb3A6UunmjOJA0qzGf6uX9WhiZVUBSxaw5vqpfpig5KCOTOD1bePExFkgBtg==",
                    SecurityStamp = "XK6PFK7W2ZPUONSDJOZBVTXYPUFHPQDT",
                    ConcurrencyStamp = "bd2957a9-09cb-432c-a9e1-eaff0ab35e2a",
                    LockoutEnabled = true,
                    DataCriacao = DateTime.Now,
                    RegistroAtivo = true
                },
                new Usuario()
                {
                    Id = 5,
                    UserName = "Matheus",
                    Nome = "Matheus",
                    Sobrenome = "Danato",
                    Email = "matheus@gmail.com",
                    NormalizedUserName = "MATHEUS",
                    Password = "a2bc101d9c97208c39cb0193aeab92849d480e8e4bacf2bb92a76fdbda4e81102718d6949dddfebdeaa8adde3c66462127f5fd49c882cac73ec2441c77519d89", //12345678
                    NormalizedEmail = "MATHEUS@GMAIL.COM",
                    PasswordHash = "AQAAAAEAACcQAAAAEIqYHm5wMYV9Q12A2XuPWbiiNpMo+MzEqkstXg6KY1ock/P+PWTe0niln9TMhpTGHQ==",
                    SecurityStamp = "DMIGEGEOFASNPKKAI53CX7YH2GTGMLXU",
                    ConcurrencyStamp = "3b49b908-94e5-40c4-ad14-03185e688858",
                    LockoutEnabled = true,
                    DataCriacao = DateTime.Now,
                    RegistroAtivo = true
                });

            #endregion

            #region cidades
            modelBuilder.Entity<Cidade>().HasData(
                 new Cidade()
                 {
                     Id = 1,
                     Nome = "Abdon Batista",
                     RegistroAtivo = true,
                     DataCriacao = DateTime.Now
                 },
                 new Cidade()
                 {
                     Id = 2,
                     Nome = "Abelardo Luz",
                     RegistroAtivo = true,
                     DataCriacao = DateTime.Now
                 },
                 new Cidade()
                 {
                     Id = 3,
                     Nome = "Agrolândia",
                     RegistroAtivo = true,
                     DataCriacao = DateTime.Now
                 },
                 new Cidade()
                 {
                     Id = 4,
                     Nome = "Agronômica",
                     RegistroAtivo = true,
                     DataCriacao = DateTime.Now
                 },
                new Cidade()
                {
                    Id = 5,
                    Nome = "Água Doce",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 6,
                    Nome = "Águas de Chapecó",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 7,
                    Nome = "Águas Frias",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 8,
                    Nome = "Águas Mornas",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 9,
                    Nome = "Alfredo Wagner",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 10,
                    Nome = "Alto Bela Vista",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 11,
                    Nome = "Anchieta",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 12,
                    Nome = "Angelina",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 13,
                    Nome = "Anita Garibaldi",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 14,
                    Nome = "Anitápolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 15,
                    Nome = "Antônio Carlos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 16,
                    Nome = "Apiúna",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 17,
                    Nome = "Arabutã",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 18,
                    Nome = "Araquari",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 19,
                    Nome = "Araranguá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 20,
                    Nome = "Armazém",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 21,
                    Nome = "Arroio Trinta",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 22,
                    Nome = "Arvoredo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 23,
                    Nome = "Ascurra",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 24,
                    Nome = "Atalanta",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 25,
                    Nome = "Aurora",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 26,
                    Nome = "Balneário Arroio do Silva",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 27,
                    Nome = "Balneário Barra do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 28,
                    Nome = "Balneário Camboriú",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 29,
                    Nome = "Balneário Gaivota",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 30,
                    Nome = "Balneário Piçarras",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 31,
                    Nome = "Balneário Rincão",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 32,
                    Nome = "Bandeirante",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 33,
                    Nome = "Barra Bonita",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 34,
                    Nome = "Barra Velha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 35,
                    Nome = "Bela Vista do Toldo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 36,
                    Nome = "Belmonte",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 37,
                    Nome = "Benedito Novo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 38,
                    Nome = "Biguaçu",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 39,
                    Nome = "Blumenau",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 40,
                    Nome = "Bocaina do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 41,
                    Nome = "Bom Jardim da Serra",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 42,
                    Nome = "Bom Jesus",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 43,
                    Nome = "Bom Jesus do Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 44,
                    Nome = "Bom Retiro",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 45,
                    Nome = "Bombinhas",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 46,
                    Nome = "Botuverá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 47,
                    Nome = "Braço do Norte",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 48,
                    Nome = "Braço do Trombudo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 49,
                    Nome = "Brunópolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 50,
                    Nome = "Brusque",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 51,
                    Nome = "Caçador",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 52,
                    Nome = "Caibi",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 53,
                    Nome = "Calmon",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 54,
                    Nome = "Camboriú",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 55,
                    Nome = "Campo Alegre",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 56,
                    Nome = "Campo Belo do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 57,
                    Nome = "Campo Erê",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 58,
                    Nome = "Campos Novos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 59,
                    Nome = "Canelinha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 60,
                    Nome = "Canoinhas",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 61,
                    Nome = "Capão Alto",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 62,
                    Nome = "Capinzal",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 63,
                    Nome = "Capivari de Baixo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 64,
                    Nome = "Catanduvas",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 65,
                    Nome = "Caxambu do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 66,
                    Nome = "Celso Ramos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 67,
                    Nome = "Cerro Negro",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 68,
                    Nome = "Chapadão do Lageado",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 69,
                    Nome = "Chapecó",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 70,
                    Nome = "Cocal do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 71,
                    Nome = "Concórdia",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 72,
                    Nome = "Cordilheira Alta",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 73,
                    Nome = "Coronel Freitas",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 74,
                    Nome = "Coronel Martins",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 75,
                    Nome = "Correia Pinto",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 76,
                    Nome = "Corupá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 77,
                    Nome = "Criciúma",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 78,
                    Nome = "Cunha Porã",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 79,
                    Nome = "Cunhataí",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 80,
                    Nome = "Curitibanos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 81,
                    Nome = "Descanso",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 82,
                    Nome = "Dionísio Cerqueira",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 83,
                    Nome = "Dona Emma",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 84,
                    Nome = "Doutor Pedrinho",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 85,
                    Nome = "Entre Rios",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 86,
                    Nome = "Ermo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 87,
                    Nome = "Erval Velho",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 88,
                    Nome = "Faxinal dos Guedes",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 89,
                    Nome = "Flor do Sertão",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 90,
                    Nome = "Florianópolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 91,
                    Nome = "Formosa do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 92,
                    Nome = "Forquilhinha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 93,
                    Nome = "Fraiburgo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 94,
                    Nome = "Frei Rogério",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 95,
                    Nome = "Galvão",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 96,
                    Nome = "Garopaba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 97,
                    Nome = "Garuva",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 98,
                    Nome = "Gaspar",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 99,
                    Nome = "Governador Celso Ramos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 100,
                    Nome = "Grão-Pará",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 101,
                    Nome = "Gravatal",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 102,
                    Nome = "Guabiruba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 103,
                    Nome = "Guaraciaba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 104,
                    Nome = "Guaramirim",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 105,
                    Nome = "Guarujá do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 106,
                    Nome = "Guatambu",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 107,
                    Nome = "Herval d'Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 108,
                    Nome = "Ibiam",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 109,
                    Nome = "Ibicaré",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 110,
                    Nome = "Ibirama",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 111,
                    Nome = "Içara",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 112,
                    Nome = "Ilhota",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 113,
                    Nome = "Imaruí",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 114,
                    Nome = "Imbituba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 115,
                    Nome = "Imbuia",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 116,
                    Nome = "Indaial",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 117,
                    Nome = "Iomerê",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 118,
                    Nome = "Ipira",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 119,
                    Nome = "Iporã do Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 120,
                    Nome = "Ipuaçu",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 121,
                    Nome = "Ipumirim",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 122,
                    Nome = "Iraceminha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 123,
                    Nome = "Irani",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 124,
                    Nome = "Irati",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 125,
                    Nome = "Irineópolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 126,
                    Nome = "Itá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 127,
                    Nome = "Itaiópolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 128,
                    Nome = "Itajaí",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 129,
                    Nome = "Itapema",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 130,
                    Nome = "Itapiranga",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 131,
                    Nome = "Itapoá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 132,
                    Nome = "Ituporanga",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 133,
                    Nome = "Jaborá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 134,
                    Nome = "Jacinto Machado",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 135,
                    Nome = "Jaguaruna",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 136,
                    Nome = "Jaraguá do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 137,
                    Nome = "Jardinópolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 138,
                    Nome = "Joaçaba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 139,
                    Nome = "Joinville",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 140,
                    Nome = "José Boiteux",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 141,
                    Nome = "Jupiá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 142,
                    Nome = "Lacerdópolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 143,
                    Nome = "Lages",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 144,
                    Nome = "Laguna",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 145,
                    Nome = "Lajeado Grande",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 146,
                    Nome = "Laurentino",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 147,
                    Nome = "Lauro Müller",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 148,
                    Nome = "Lebon Régis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 149,
                    Nome = "Leoberto Leal",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 150,
                    Nome = "Lindóia do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 151,
                    Nome = "Lontras",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 152,
                    Nome = "Luiz Alves",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 153,
                    Nome = "Luzerna",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 154,
                    Nome = "Macieira",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 155,
                    Nome = "Mafra",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 156,
                    Nome = "Major Gercino",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 157,
                    Nome = "Major Vieira",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 158,
                    Nome = "Maracajá",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 159,
                    Nome = "Maravilha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 160,
                    Nome = "Marema",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 161,
                    Nome = "Massaranduba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 162,
                    Nome = "Matos Costa",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 163,
                    Nome = "Meleiro",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 164,
                    Nome = "Mirim Doce",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 165,
                    Nome = "Modelo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 166,
                    Nome = "Mondaí",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 167,
                    Nome = "Monte Carlo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 168,
                    Nome = "Monte Castelo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 169,
                    Nome = "Morro da Fumaça",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 170,
                    Nome = "Morro Grande",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 171,
                    Nome = "Navegantes",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 172,
                    Nome = "Nova Erechim",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 173,
                    Nome = "Nova Itaberaba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 174,
                    Nome = "Nova Trento",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 175,
                    Nome = "Nova Veneza",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 176,
                    Nome = "Novo Horizonte",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 177,
                    Nome = "Orleans",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 178,
                    Nome = "Otacílio Costa",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 179,
                    Nome = "Ouro",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 180,
                    Nome = "Ouro Verde",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 181,
                    Nome = "Paial",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 182,
                    Nome = "",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 183,
                    Nome = "Painel",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 184,
                    Nome = "Palhoça",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 185,
                    Nome = "Palma Sola",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 186,
                    Nome = "Palmeira",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 187,
                    Nome = "Palmitos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 188,
                    Nome = "Papanduva",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 189,
                    Nome = "Paraíso",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 190,
                    Nome = "Passo de Torres",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 191,
                    Nome = "Passos Maia",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 192,
                    Nome = "Paulo Lopes",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 193,
                    Nome = "Pedras Grandes",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 194,
                    Nome = "Penha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 195,
                    Nome = "Peritiba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 196,
                    Nome = "Pescaria Brava",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 197,
                    Nome = "Petrolândia",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 198,
                    Nome = "Pinhalzinho",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 199,
                    Nome = "Pinheiro Preto",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 200,
                    Nome = "Piratuba",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 201,
                    Nome = "Planalto Alegre",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 202,
                    Nome = "Pomerode",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 203,
                    Nome = "Ponte Alta",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 204,
                    Nome = "Ponte Alta do Norte",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 205,
                    Nome = "Ponte Serrada",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 206,
                    Nome = "Porto Belo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 207,
                    Nome = "Porto União",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 208,
                    Nome = "Pouso Redondo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 209,
                    Nome = "Praia Grande",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 210,
                    Nome = "Presidente Castello Branco",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 211,
                    Nome = "Presidente Getúlio",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 212,
                    Nome = "Presidente Nereu",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 213,
                    Nome = "Princesa",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 214,
                    Nome = "Quilombo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 215,
                    Nome = "Rancho Queimado",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 216,
                    Nome = "Rio das Antas",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 217,
                    Nome = "Rio do Campo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 218,
                    Nome = "Rio do Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 219,
                    Nome = "Rio do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 220,
                    Nome = "Rio dos Cedros",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 221,
                    Nome = "Rio Fortuna",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 222,
                    Nome = "Rio Negrinho",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 223,
                    Nome = "Rio Rufino",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 224,
                    Nome = "Riqueza",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 225,
                    Nome = "Rodeio",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 226,
                    Nome = "Romelândia",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 227,
                    Nome = "Salete",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 228,
                    Nome = "Saltinho",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 229,
                    Nome = "Salto Veloso",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 230,
                    Nome = "Sangão",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 231,
                    Nome = "Santa Cecília",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 232,
                    Nome = "Santa Helena",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 233,
                    Nome = "Santa Rosa de Lima",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 234,
                    Nome = "Santa Rosa do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 235,
                    Nome = "Santa Terezinha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 236,
                    Nome = "Santa Terezinha do Progresso",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 237,
                    Nome = "Santiago do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 238,
                    Nome = "Santo Amaro da Imperatriz",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 239,
                    Nome = "São Bento do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 240,
                    Nome = "São Bernardino",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 241,
                    Nome = "São Bonifácio",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 242,
                    Nome = "São Carlos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 243,
                    Nome = "São Cristóvão do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 244,
                    Nome = "São Domingos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 245,
                    Nome = "São Francisco do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 246,
                    Nome = "São João Batista",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 247,
                    Nome = "São João do Itaperiú",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 248,
                    Nome = "São João do Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 249,
                    Nome = "São João do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 250,
                    Nome = "São Joaquim",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 251,
                    Nome = "São José",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 252,
                    Nome = "São José do Cedro",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 253,
                    Nome = "São José do Cerrito",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 254,
                    Nome = "São Lourenço do Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 255,
                    Nome = "São Ludgero",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 256,
                    Nome = "São Martinho",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 257,
                    Nome = "São Miguel da Boa Vista",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 258,
                    Nome = "São Miguel do Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 259,
                    Nome = "São Pedro de Alcântara",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 260,
                    Nome = "Saudades",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 261,
                    Nome = "Schroeder",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 262,
                    Nome = "Seara",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 263,
                    Nome = "Serra Alta",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 264,
                    Nome = "Siderópolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 265,
                    Nome = "Sombrio",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 266,
                    Nome = "Sul Brasil",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 267,
                    Nome = "Taió",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 268,
                    Nome = "Tangará",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 269,
                    Nome = "Tigrinhos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 270,
                    Nome = "Tijucas",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 271,
                    Nome = "Timbé do Sul",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 272,
                    Nome = "Timbó",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 273,
                    Nome = "Timbó Grande",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 274,
                    Nome = "Três Barras",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 275,
                    Nome = "Treviso",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 276,
                    Nome = "Treze de Maio",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 277,
                    Nome = "Treze Tílias",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 278,
                    Nome = "Trombudo Central",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 279,
                    Nome = "Tubarão",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 280,
                    Nome = "Tunápolis",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 281,
                    Nome = "Turvo",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 282,
                    Nome = "União do Oeste",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 283,
                    Nome = "Urubici",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 284,
                    Nome = "Urupema",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 285,
                    Nome = "Urussanga",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 286,
                    Nome = "Vargeão",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 287,
                    Nome = "Vargem",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 288,
                    Nome = "Vargem Bonita",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 289,
                    Nome = "Vidal Ramos",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 290,
                    Nome = "Videira",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 291,
                    Nome = "Vitor Meireles",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 292,
                    Nome = "Witmarsum",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 293,
                    Nome = "Xanxerê",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 294,
                    Nome = "Xavantina",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 295,
                    Nome = "Xaxim",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Cidade()
                {
                    Id = 296,
                    Nome = "Zortéa",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                }
                );
            #endregion

            #region montanha
            modelBuilder.Entity<Montanha>().HasData(
        new Montanha()
        {

            Id = 1,
            IdCidade = 139,
            Nome = "Castelo dos Bugres",
            Altitude = 970,
            Extensao = 4,
            Duracao = new DateTime(2000, 02, 02, 02, 02, 02),
            Nivel = "Fácil",
            IdadeMinima = 0,
            Descricao = "É chamado de Castelo dos Bugres uma formação de rochas sobrepostas, no alto da Serra do Mar, que pode ser apreciada a partir da rodovia Dona Francisca. O destino atrai montanhistas interessados em escalada ou rapel, e aventureiros que querem apreciar a vista e desfrutar de uma pequena caverna no local. O percurso é marcado por mananciais de água, rochas e a beleza estonteante da Mata Atlântica, percorrendo as nascentes do rio Piraí. O nível de dificuldade é considerado fácil, pois a trilha é bem marcada e aberta. A dica é evitar os dias de chuva e cuidar com as bifurcações, pois é possível se perder.",
            Latitude = "-26.234753",
            Longitude = "-49.048240",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 2,
            IdCidade = 97,
            Nome = "Monte Crista",
            Altitude = 967,
            Extensao = 8,
            Duracao = new DateTime(2000, 04, 04, 04, 04, 04),
            Nivel = "Moderado/Difícil",
            IdadeMinima = 0,
            Descricao = "O Monte Crista é cercado por uma beleza magnífica e muita história. É uma formação rochosa de puro granito e do alto de seu cume é possível vislumbrar vales extensos com árvores centenárias e natureza intocada. Em suas encostas, passam os rios Três Barras e o Crista, com suas cascatas, lajes de granito e pequenos lagos de águas geladas e cristalinas. Conta que o caminho ao alto já foi utilizado por padres jesuítas há mais de 200 anos, existindo escadarias com mais de mil anos de existência e ainda bem conservadas, há referências ao local como “Caminho dos Ambrósios”, utilizado para cruzar a Serra do Mar desde Curitiba até a Baia da Babitonga – ainda pode ter sido aberto seguindo um ramal do Caminho de Peabiru, que se liga ao Peru.Segundo o guia Rossano Tribes, devido à grande quantidade de pessoas que frequenta o local, a trilha é bem marcada. Mas é preciso preparo físico para encarar a distância e pontos difíceis.",

            Latitude = "-26.080870",
            Longitude = "-48.919780",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 3,
            IdCidade = 283,
            Nome = "Morro da Igreja",
            Altitude = 847,
            Extensao = 3,
            Duracao = new DateTime(2000, 02, 02, 02, 02, 02),
            Nivel = "Moderado/Fácil",
            IdadeMinima = 0,
            Descricao = "Conforme o guia Carlos Reinke, o aceso até o pé do morro pode ser feito por São Bento do Sul, passando pelo bairro Rio Vermelho Estação. Ao chegar ao cume é possível tem uma visão panorâmica da Serra de São Bento, Corupá e Jaraguá do Sul. “A entrada para a trilha é em propriedade particular. É tradicional a existência de uma cruz de madeira no topo do morro, e recentemente recebeu uma cruz em metal”, comenta.",

            Latitude = "-26.364364",
            Longitude = "-49.258283",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 4,
            IdCidade = 28,
            Nome = "Morro do Boi",
            Altitude = 820,
            Extensao = 3,
            Duracao = new DateTime(2000, 02, 02, 02, 02, 02),
            Nivel = "Moderado",
            IdadeMinima = 0,
            Descricao = "Em meio à natureza tão abundante de Corupá, está um lugar muito procurado por quem estima sentir a imponência da Mata Atlântica. O imponente Morro do Boi tem sua trilha localizada no Ano Bom. “A trilha já consolidada é relativamente bem marcada, podendo estar fechada em alguns pontos, sendo necessário bastante atenção a descida íngreme, principalmente na parte próxima ao cume”, conta o guia Rossano Tribes.",

            Latitude = "-26.426844",
            Longitude = "-49.230876",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 5,
            IdCidade = 97,
            Nome = "Pedra da Tartaruga",
            Altitude = 1350,
            Extensao = 7.8M,
            Duracao = new DateTime(2000, 06, 06, 06, 06, 06),
            Nivel = "Moderado",
            IdadeMinima = 0,
            Descricao = "O conjunto de montanhas que compreende o Quiriri possui cerca de 30 cumes, cuja altura varia entre 1.300 a 1.580 metros. O cenário lembra filmes medievais, com uma beleza incomparável. De acordo com o guia Carlos Reinke, a entrada de acesso para é pelas Serra Dona Francisca - o local faz parte de uma Área de Proteção Ambiental, que compreende os municípios de Campo Alegre, Garuva e Joinville. “Até chegar na fazenda de acesso, da 50km de estrada de chão. Para ir para fazenda dos Campos do Quiriri de 4x4 ou fazer trekking precisa de autorização”, comenta. A trilha indicada, comenta Carlos, vai do portão do portão da fazenda até a Pedra da Tartaruga.“É necessário ir com pessoas que já fizeram esse trekking, acompanhados de um GPS pois as condições climáticas mudam rapidamente”, comenta. Quiriri na língua tupi guarani significa “Silêncio Noturno”, e contam os antigos que ali era morada de índios e bugres.",

            Latitude = "-26.012465",
            Longitude = "-48.923789",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 6,
            IdCidade = 136,
            Nome = "Pico Jaraguá",
            Altitude = 926,
            Extensao = 2.7M,
            Duracao = new DateTime(2000, 02, 02, 02, 02, 02),
            Nivel = "Moderado",
            IdadeMinima = 0,
            Descricao = "O Pico, conforme o guia Carlos Reinke, está no maciço conhecido por “Morro Boa Vista”, que ainda possui o Morro do Meio e o Morro das Antenas. O acesso a trilha é pelo bairro Águas Claras, porém, a entrada precisa de uma autorização por passar na fazenda da família Krause. Também é possível chegar ao local a partir do Morro das Antenas, existe uma trilha que atravessa os cumes.",

            Latitude = "-26.515440",
            Longitude = "-49.061781",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 7,
            IdCidade = 139,
            Nome = "Pico Jurapê",
            Altitude = 1149,
            Extensao = 6,
            Duracao = new DateTime(2000, 04, 04, 04, 04, 04),
            Nivel = "Difícil",
            IdadeMinima = 0,
            Descricao = "De acordo com informações do site Terra Média Trekking, o Jurapê é uma das montanhas mais exigentes de Santa Catarina. A trilha é relativamente bem marcada, possuindo inclusive algumas placas indicativas em certas bifurcações, mas ainda assim há trechos que exigem atenção para não se perder. “O trecho final da trilha é bastante íngreme, exigindo bastante preparo físico”, destaca o guia Rossano Tribes. Com a história oficial que no de junho de 1886, o imigrante suíço Johan Paul Schmalz, juntamente com Bruno Clauser, Hahn, Jacob Schmalz, Otto Delitsch e mais dois sujeitos definidos como “dois alugados”, atingiram o ponto culminante da montanha, após três dias abrindo a trilha.",

            Latitude = "-26.259292",
            Longitude = "-49.008120",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },

        new Montanha()
        {
            Id = 8,
            IdCidade = 209,
            Nome = "Rio do Boi",
            Altitude = 0,
            Extensao = 14,
            Duracao = new DateTime(2000, 06, 06, 06, 06, 06),
            Nivel = "Moderado/Difícil",
            IdadeMinima = 18,
            Descricao = "O Rio do Boi é o rio que forma o Canyon Itaimbezinho, e configura uma  trilha de aventura e  de beleza impressionantes, pois se caminha por dentro da grande garganta do  Canyon entre paredões de até 720m. A origem do nome Rio do Boi surgiu no tempo das tropas que desciam os campos de cima da serra abrindo caminho montanha abaixo, pelas íngremes escarpas da serra geral. Diz a lenda que em uma das' tropeadas' um boi se desgarrou da tropa e caiu no penhasco que culminava no rio de pedras que cortava a montanha. Esta é uma das versões da origem do nome do Rio do Boi.",

            Latitude = "-29.201767",
            Longitude = "-50.042767",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 9,
            IdCidade = 209,
            Nome = "Canyon Malacara",
            Altitude = 0,
            Extensao = 5,
            Duracao = new DateTime(2000, 04, 04, 04, 04, 04),
            Nivel = "Moderado/Difícil",
            IdadeMinima = 18,
            Descricao = "O Cânion Malacara é outro lindo cânion do Parque Nacional da Serra Geral, com 3,5km de extensão e com largura máxima de 1000m e profundidade de 780m, compõe um ambiente que reúne campos de altitude recortados por pequenos arroios de águas rasas e límpidas que correm em todas as direções possíveis. Guarda consigo ecos de tempos distantes deixados pelos tropeiros que, conduziam gado, porcos e mulas carregadas de produtos serranos, descendo as encostas da serra para negociar e retornando com novos produtos e histórias. Seu nome originou-se de uma pedra em uma das paredes que lembra o formato de um cavalo malacara, que independente da raça, possui uma listra branca na parte frontal de sua cabeça.",

            Latitude = "-29.159044",
            Longitude = "-49.981984",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 10,
            IdCidade = 275,
            Nome = "Trilha Dois Dedos",
            Altitude = 624,
            Extensao = 6,
            Duracao = new DateTime(2000, 02, 02, 02, 02, 02),
            Nivel = "Difícil",
            IdadeMinima = 18,
            Descricao = "Localizado em Treviso, sul de Santa Catarina, o morro foi apelidado de dois dedos pois para quem olha de baixo é a impressão que tem devido à formação das duas rochas em seu topo.",

            Latitude = "-28.511404",
            Longitude = "-49.525824",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 11,
            IdCidade = 39,
            Nome = "Spitzkopf",
            Altitude = 936,
            Extensao = 6,
            Duracao = new DateTime(2000, 04, 04, 04, 04, 04),
            Nivel = "Fácil",
            IdadeMinima = 18,
            Descricao = "O morro Spitzkopf (numa tradução literal do alemão cabeça pontuda) é uma montanha no município brasileiro de Blumenau, com 936 metros de altitude ( a altura oficial pelo IBGE é de 913 metros e 98 cm). Situa-se no Parque Ecológico Spitzkopf, cuja área é de mais de 5.000 m² de Mata Atlântica, piscinas naturais, cascatas e riachos. O morro Spitzkopf foi escalado pela primeira vez, em 1872, pelo comandante das Guardas de Batedores do Mato - Friedrich Deeke (oficialmente investido na função com o nome aportuguesado: Frederico Deeke). Em 19 e 20 de julho de 1892 a montanha foi escalada pelos excursionistas Otto Wehmuth, Christian Imroth, Fritz Alfarht e outros. Em 17 de julho de 1929, foi criado o Spitzkopf - Klub, tendo como diretor Otto Huber, secretário Rudolf Hollenweger, cobrador Alfredo Gossweiler, rancheiro, guarda da cabana, Fritz Hasse. Proprietários Paul Scheidemantel, Gauche (alfaiate) e Wünsch. Sobre o Spitzkopf há farta literatura contida na Revista Blumenau em Cadernos, entretanto merece especial referência o fato de haver sido escolhido pelo professor Max Humpl para lá, numa altitude de 750 metros (segundo consta do diário de Max Humpl sua casa situar-se-ia aos 500 metros), quase no topo da montanha de 938 metros, construir sua residência.",

            Latitude = "-27.025026",
            Longitude = "-49.132220",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 12,
            IdCidade = 39,
            Nome = "Morro do Sapo",
            Altitude = 718,
            Extensao = 4.5M,
            Duracao = new DateTime(2000, 03, 03, 03, 03, 03),
            Nivel = "Fácil/Moderado",
            IdadeMinima = 18,
            Descricao = "O morro está localizado dentro do Parque das Nascentes, que faz parte do Parque Nacional da Serra do Itajaí (PNSI), o qual compreende uma área de 57.374 hectares e altitudes de 80 a 1039 metros, abrangendo nove municípios de Santa Catarina – Indaial, Blumenau, Botuverá, Gaspar, Vidal Ramos, Apiúna, Guabiruba, Ascurra e Presidente Nereu. Também na sede do parque há a trilha da chuva, com 2,7 km de extensão e diversas travessias de rio. O acesso ao local é controlado pelo Instituto Parque das Nascentes.",

            Latitude = "-27.069194",
            Longitude = "-49.106680",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        },
        new Montanha()
        {
            Id = 13,
            IdCidade = 184,
            Nome = "O Cambirela",
            Altitude = 915,
            Extensao = 2.8M,
            Duracao = new DateTime(2000, 03, 03, 03, 03, 03),
            Nivel = "Fácil/Moderado/Difícil",
            IdadeMinima = 18,
            Descricao = "Morro do Cambirela localiza-se no município de Palhoça/SC, o morro faz parte de um conjunto de montanhas pertencentes ao Parque Estadual da Serra do Tabuleiro, sendo esse parque a maior unidade de conservação de proteção integral do estado de Santa Catarina – Brasil. O Morro do Cambirela situa-se próximo a BR – 101, uma montanha que eleva-se a um pouco mais de 900 metros de altitude em relação ao nível do mar.",

            Latitude = "-27.755497",
            Longitude = "-48.675000",
            RegistroAtivo = true,
            DataCriacao = DateTime.Now
        }

    );
            #endregion            

            #region categoria
            modelBuilder.Entity<Categoria>().HasData(
                new Categoria()
                {
                    Id = 1,
                    Nome = "Trilha",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Categoria()
                {
                    Id = 2,
                    Nome = "Morro",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Categoria()
                {
                    Id = 3,
                    Nome = "Escalada",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Categoria()
                {
                    Id = 4,
                    Nome = "Rapel",
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                }
                );
            #endregion

            #region MontanhaCategoria
            modelBuilder.Entity<MontanhaCategoria>().HasData(
                new MontanhaCategoria()
                {
                    Id = 1,
                    IdCategoria = 1,
                    IdMontanha = 1,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 111,
                    IdCategoria = 3,
                    IdMontanha = 1,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 1111,
                    IdCategoria = 4,
                    IdMontanha = 1,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 2,
                    IdCategoria = 1,
                    IdMontanha = 2,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 3,
                    IdCategoria = 2,
                    IdMontanha = 3,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 4,
                    IdCategoria = 2,
                    IdMontanha = 4,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 5,
                    IdCategoria = 1,
                    IdMontanha = 5,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 6,
                    IdCategoria = 1,
                    IdMontanha = 6,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 7,
                    IdCategoria = 1,
                    IdMontanha = 7,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                     Id = 8,
                     IdCategoria = 1,
                     IdMontanha = 8,
                     RegistroAtivo = true,
                     DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                      Id = 9,
                      IdCategoria = 1,
                      IdMontanha = 9,
                      RegistroAtivo = true,
                      DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 99,
                    IdCategoria = 4,
                    IdMontanha = 9,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 10,
                    IdCategoria = 1,
                    IdMontanha = 10,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 11,
                    IdCategoria = 2,
                    IdMontanha = 11,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 12,
                    IdCategoria = 1,
                    IdMontanha = 12,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new MontanhaCategoria()
                {
                    Id = 13,
                    IdCategoria = 1,
                    IdMontanha = 13,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                }
            );
            #endregion

            #region Comentarios
            modelBuilder.Entity<Comentario>().HasData(
                new Comentario()
                {
                    Id = 1,
                    IdMontanha = 1,
                    IdUsuario = 1,
                    ComentarioTexto = "Essa Montanha é Top",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 2,
                    IdMontanha = 1,
                    IdUsuario = 2,
                    ComentarioTexto = "Essa Montanha é Muito Top",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 3,
                    IdMontanha = 1,
                    IdUsuario = 2,
                    ComentarioTexto = "Essa Montanha é Muito Muito Top",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 4,
                    IdMontanha = 2,
                    IdUsuario = 3,
                    ComentarioTexto = "Ja subi essa montanha foi top",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 5,
                    IdMontanha = 3,
                    IdUsuario = 4,
                    ComentarioTexto = "Ja subi essa montanha foi top de mais",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 6,
                    IdMontanha = 4,
                    IdUsuario = 5,
                    ComentarioTexto = "Ja subi essa montanha foi top de mais",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 7,
                    IdMontanha = 5,
                    IdUsuario = 5,
                    ComentarioTexto = "Nunca subi mas ja tive vontade",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 8,
                    IdMontanha = 4,
                    IdUsuario = 4,
                    ComentarioTexto = "Foi legal passar um tempo com a familia",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                },
                new Comentario()
                {
                    Id = 9,
                    IdMontanha = 4,
                    IdUsuario = 4,
                    ComentarioTexto = "Fui sábado passado, foi legal",
                    DataComentario = DateTime.Now,
                    RegistroAtivo = true,
                    DataCriacao = DateTime.Now
                }
                );
            #endregion
        }
    }
}