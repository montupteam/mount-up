﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Repository.Interface;
using PusherServer;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace MontUp_API.Controllers
{
    [Route("api/chat")]
    [ApiController]
    public class ChatController : Controller
    {
        private IChatRepository _chatRepository;

        public ChatController(IChatRepository chatRepository)
        {
            _chatRepository = chatRepository;
        }

        [HttpPost, Route("cadastro")]
        public JsonResult Cadastro(Chat chat)
        {
            _chatRepository.Add(chat);
            return Json(chat);
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _chatRepository.Delete(id);
            return Json(new { status = apagou });
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var chat = _chatRepository.ObterPeloId(id);

            if (chat == null)
            {
                return NotFound();
            }

            return Json(chat);
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos()
        {
            List<Chat> chats = _chatRepository.ObterTodos();
            return Json(chats);
        }

        [HttpGet, Route("helloworld"), AllowAnonymous]
        public async Task<ActionResult> HelloWorld()
        {
            var options = new PusherOptions
            {
                Cluster = "us2",
                Encrypted = true
            };

            var pusher = new Pusher(
              "861653",
              "f63cf0218426a28c647e",
              "8286d111e2bf60f3ab8a",
              options);

            var result = await pusher.TriggerAsync(
              "my-channel",
              "my-event",
              new { message = "hello world" });

            return Ok();
        }


    }
}