﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using Model;
using Model.ViewModel;
using Repository.Interface;
using Repository.Repositories;
using SendGrid.Helpers.Mail;

namespace MontUp_API.Controllers
{
    [Route("api/usuario")]
    [Authorize]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly string _nomePasta;
        private readonly string _caminho;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly UserManager<Usuario> _userManager;

        public UsuarioController(IUsuarioRepository usuarioRepository, IHostingEnvironment env, UserManager<Usuario> userManager)
        {
            _usuarioRepository = usuarioRepository;
            _userManager = userManager;

            string wwwroot = env.WebRootPath;
            _nomePasta = "arquivos-usuario";
            _caminho = Path.Combine(wwwroot, _nomePasta);

            if (!Directory.Exists(_caminho))
            {
                Directory.CreateDirectory(_caminho);
            }
        }

        [HttpPost, Route("upload")]
        public async Task<ActionResult> Upload(IFormFile usuarioArquivo)
        {
            var nomeArquivo = usuarioArquivo.FileName;
            var nomeHash = ObterHashDoNomeDoArquivo(nomeArquivo);

            var idUsuario = (await _userManager.GetUserAsync(User)).Id;
            var usuario = _usuarioRepository.ObterPeloId(idUsuario);

            var caminhoArquivo = Path.Combine(_caminho, nomeHash);
            using (var stream = new FileStream(caminhoArquivo, FileMode.Create))
            {
                usuarioArquivo.CopyTo(stream);

                usuario.ImagemUsuario = nomeArquivo;
                usuario.ImagemHash = nomeHash;

                _usuarioRepository.Update(usuario);
            }
            return Json(usuarioArquivo);
        }

        private string CriptografiaSenha(string valor)
        {
            var _stringHash = "";
            try
            {
                UnicodeEncoding _encode = new UnicodeEncoding();
                byte[] _hashBytes, _messageBytes = _encode.GetBytes(valor);

                SHA512Managed _sha512Managed = new SHA512Managed();

                _hashBytes = _sha512Managed.ComputeHash(_messageBytes);

                foreach (byte b in _hashBytes)
                {
                    _stringHash += String.Format("{0:x2}", b);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _stringHash;
        }

        [HttpPost, Route("cadastro")]
        public ActionResult Cadastro(Usuario usuario)
        {
            _usuarioRepository.Add(usuario);
            usuario.Password = CriptografiaSenha(usuario.Password);
            return Json(usuario);
        }

        [HttpPut, Route("alterar")]
        public async Task<ActionResult> Alterar(UsuarioAlterarViewModel usuarioAlterarView)
        {
            var idUsuario = (await _userManager.GetUserAsync(User)).Id;
            var usuario = _usuarioRepository.ObterPeloId(idUsuario);

            usuario.UserName = usuarioAlterarView.UserName;
            usuario.Sobrenome = usuarioAlterarView.Sobrenome;
            usuario.Cpf = usuarioAlterarView.Cpf;
            usuario.Email = usuarioAlterarView.Email;
            usuario.NormalizedEmail = usuarioAlterarView.Email.ToUpper();
            usuario.NormalizedUserName = usuarioAlterarView.UserName.ToUpper();
            usuario.DataNascimento = usuarioAlterarView.DataNascimento;
            usuario.RegistroAtivo = usuarioAlterarView.RegistroAtivo;
            usuario.DataCriacao = usuarioAlterarView.DataCriacao;

            _usuarioRepository.Update(usuario);
            return Json(usuario);
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _usuarioRepository.Delete(id);
            return Json(new { status = apagou });
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos()
        {
            List<Usuario> usuarios = _usuarioRepository.ObterTodos();
            return Json(usuarios);
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var usuario = _usuarioRepository.ObterPeloId(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return Json(usuario);
        }

        [HttpGet, Route("obterusuariologado")]
        public ActionResult ObterPeloId()
        {
            var id =  _userManager.GetUserAsync(User).Result.Id;
            var usuario = _usuarioRepository.ObterPeloId(id);

            if (usuario == null)
            {
                return NotFound();
            }

            return Json(usuario);
        }


        [AllowAnonymous]
        [HttpGet, Route("obterimagemusuario")]
        public ActionResult ObterImagemUsuario(int id)
        {
            var imagemUsuario = _usuarioRepository.ObterPeloId(id);

            if (imagemUsuario == null)
            {
                return NotFound();
            }

            return File(new FileStream(Path.Combine(_caminho, imagemUsuario.ImagemHash == null ? "profile.jpg" : imagemUsuario.ImagemHash), FileMode.Open), "image/jpeg");
        }

        public static string ObterHashDoNomeDoArquivo(string nome)
        {
            FileInfo info = new FileInfo(nome);

            var crypt = new SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(info.Name.Replace(info.Extension, "") + DateTime.Now));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return (hash + info.Extension).ToUpper();
        }
    }
}
