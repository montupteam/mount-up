﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/local")]
    [ApiController]
    public class LocalController : Controller
    {
        private ILocalRepository _localRepository;

        public LocalController(ILocalRepository localRepository)
        {
            _localRepository = localRepository;
        }

        [HttpPost, Route("cadastro")]
        public JsonResult Cadastro(Local local)
        {
            _localRepository.Add(local);
            return Json(local);
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _localRepository.Delete(id);
            return Json(new { status = apagou });
        }

        [HttpPut, Route("alterar")]
        public JsonResult Alterar(Local local)
        {
            bool alterou = _localRepository.Update(local);
            return Json(new { status = alterou });
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos()
        {
            List<Local> locais = _localRepository.ObterTodos();
            return Json(locais);
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var local = _localRepository.ObterPeloId(id);

            if(local == null)
            {
                return NotFound();
            }

            return Json(local);
        }

    }
}