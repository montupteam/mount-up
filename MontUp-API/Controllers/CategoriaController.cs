﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/categoria")]
    [ApiController]
    public class CategoriaController : Controller
    {
        private readonly IBaseReadRepository _baseReadRepository;
        private readonly IBaseRepository _baseRepository;
        public CategoriaController(IBaseRepository baseRepository, IBaseReadRepository baseReadRepository)
        {
            _baseReadRepository = baseReadRepository;
            _baseRepository = baseRepository;
        }

        [HttpPost, Route("cadastro")]
        public JsonResult Inserir(Categoria categoria)
        {
            return Json(_baseRepository.Add<Categoria>(categoria));
        }

        [HttpPut, Route("alterar")]
        public JsonResult Alterar(Categoria categoria)
        {
            return Json(_baseRepository.Update<Categoria>(categoria));
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            return Json(_baseRepository.Delete<Categoria>(id));
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos()
        {
            return Json(_baseReadRepository.ObterTodos<Categoria>());
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var categoria = _baseReadRepository.ObterPeloId<Categoria>(id);

            if(categoria == null)
            {
                return NotFound();
            }

            return Json(categoria);
        }
    }
}