﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Model;
using MontUp_API.Dto;
using Repository.Interface;

namespace MontUp_API.Controllers
{

    [Route("api/autenticacao")]
    [ApiController]
    [Authorize]
    public class AutenticacaoController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly UserManager<Usuario> _userManager;
        private readonly SignInManager<Usuario> _signInManager;
        private readonly IMapper _mapper;
        public AutenticacaoController(IConfiguration config,
                                      UserManager<Usuario> userManager,
                                      SignInManager<Usuario> signInManager,
                                      IMapper mapper)
        {
            _config = config;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        private string CriptografiaSenha(string valor)
        {
            var _stringHash = "";
            try
            {
                UnicodeEncoding _encode = new UnicodeEncoding();
                byte[] _hashBytes, _messageBytes = _encode.GetBytes(valor);

                SHA512Managed _sha512Managed = new SHA512Managed();

                _hashBytes = _sha512Managed.ComputeHash(_messageBytes);

                foreach (byte b in _hashBytes)
                    _stringHash += String.Format("{0:x2}", b);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _stringHash;
        }

        [HttpGet("GetUser")]
        public IActionResult GetUser()
        {
            return Ok(new UsuarioDto());
        }

        [HttpPost("cadastrar")]
        [AllowAnonymous]
        public async Task<IActionResult> Cadastrar(UsuarioDto usuarioDto)
        {
            try
            {
                var user = _mapper.Map<Usuario>(usuarioDto);
                user.DataCriacao = DateTime.Now;
                user.RegistroAtivo = true;
                user.Password = CriptografiaSenha(usuarioDto.Password);

                MailMessage mail = new MailMessage();

                if (usuarioDto.ConfirmarEmail == false)
                {
                    mail.From = new MailAddress("montupbnusc@gmail.com");
                    mail.To.Add(usuarioDto.Email);
                    mail.Subject = "Confirmação de Email MontUp!";
                    mail.IsBodyHtml = true;
                    mail.Body = @"
<body>
  <table cellpadding='0' cellspacing='0' border='0' width='100%' style='background: #f5f8fa; min-width: 350px; font-size: 1px; line-height: normal;'>
  <tr>
    <td align='center' valign='top'>
            <table cellpadding='0' cellspacing='0' border='0' width='750' class='table750'
            style='width: 100%; max-width: 750px; min-width: 350px; background: #f5f8fa;'>
              <tr>
                <td class='mob_pad' width='25' style='width: 25px; max-width: 25px; min-width: 25px;'>&nbsp;</td>
                <td align='center' valign='top' style='background: #ffffff;'>
                  <table cellpadding='0' cellspacing='0' border='0' width='100%' style='width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;'>
                    <tr>
                      <td align='right' valign='top'>
                        <div class='top_pad' style='height: 25px; line-height: 25px; font-size: 23px;'>&nbsp;</div>
                      </td>
                    </tr>
                  </table>
                  <table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                    <tr>
                      <td align='center' valign='top'>
                        <div style='height: 40px; line-height: 40px; font-size: 38px;'>&nbsp;</div>
                         <img src='https://scontent.fbnu1-1.fna.fbcdn.net/v/t1.0-9/70263013_3219794891378866_2299708644863770624_n.jpg?_nc_cat=109&_nc_oc=AQl2Jlo34UkMTsxP6ERtopHhbeCKF3SR_NmEo02xL5IcITvbXTY0bPpc3IMOeDx4-oE&_nc_ht=scontent.fbnu1-1.fna&oh=ae69c672b1cde35546952df847920da4&oe=5DFF6ECB'>
                        </a>
                        <div class='top_pad2' style='height: 48px; line-height: 48px; font-size: 46px;'>&nbsp;</div>
                      </td>
                    </tr>
                  </table>
                  <table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                    <tr>
                      <td align='left' valign='top'> <font face=''Source Sans Pro', sans-serif' color='#1a1a1a' style='font-size: 45px; line-height: 54px; font-weight: 300; letter-spacing: -1.5px;'>
                              <span style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 45px; line-height: 54px; font-weight: 300; letter-spacing: -1.5px;'>Confirme Seu Email</span>
                           </font>

                        <div style='height: 21px; line-height: 21px; font-size: 19px;'>&nbsp;</div> <font face=''Source Sans Pro', sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                              <span style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'>
                              Olá " + user.UserName + " " + user.Sobrenome + @"
                              </span>
                           </font>

                        <div style='height: 6px; line-height: 6px; font-size: 4px;'>&nbsp;</div> <font face=''Source Sans Pro', sans-serif' color='#000000' style='font-size: 20px; line-height: 28px;'>
                              <span style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #000000; font-size: 20px; line-height: 28px;'>
                                Recebemos uma solicitação de confirmação de email na MontUp. 
                                <br>
                                Se foi você que solicitou, confirme clicando no botão abaixo.
                              </span>
                           </font>

                        <div style='height: 60px; line-height: 30px; font-size: 28px;'>&nbsp;</div>
                        <table class='mob_btn' cellpadding='0' cellspacing='0' border='0'
                        style='background: rgb(58, 181, 238); border-radius: 4px;'>
                          <tr>
                            <td align='center' valign='top'>
                              <a href='http://montup.azurewebsites.net/#/autenticacao/login'
                              target='_blank' style='display: block; border: 1px solid rgb(58, 181, 238); border-radius: 4px; padding: 19px 27px; font-family: Source Sans Pro, Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 26px; line-height: 30px; text-decoration: none; white-space: nowrap; font-weight: 600;'> <font face=''Source Sans Pro', sans-serif' color='#ffffff' style='font-size: 26px; line-height: 30px; text-decoration: none; white-space: nowrap; font-weight: 600;'>
               <span style='font-family: 'Source Sans Pro', Arial, Verdana, Tahoma, Geneva, sans-serif; color: #ffffff; font-size: 26px; line-height: 30px; text-decoration: none; white-space: nowrap; font-weight: 600;'>Confirmar Email</span>
            </font>
                              </a>
                            </td>
                          </tr>
                        </table>
                        <div style='height: 90px; line-height: 90px; font-size: 88px;'>&nbsp;</div>
                      </td>
                    </tr>
                  </table>
                  <table cellpadding='0' cellspacing='0' border='0' width='90%' style='width: 90% !important; min-width: 90%; max-width: 90%; border-width: 1px; border-style: solid; border-color: #e8e8e8; border-bottom: none; border-left: none; border-right: none;'>
                    <tr>
                      <td align='left' valign='top'>
                        <div style='height: 28px; line-height: 28px; font-size: 26px;'>&nbsp;</div>
                      </td>
                    </tr>
                  </table>
                  <table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                    <tr>
                      <td align='center' valign='top'> <font face=''Source Sans Pro', sans-serif' color='#7f7f7f' style='font-size: 17px; line-height: 23px;'>
                              <span style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #7f7f7f; font-size: 17px; line-height: 23px;'>Após a confirmação do email, seu login na Montup será liberado.
                           </font>

                        <div style='height: 30px; line-height: 30px; font-size: 28px;'>&nbsp;</div>
                      </td>
                    </tr>
                  </table>
                  <table cellpadding='0' cellspacing='0' border='0' width='100%' style='width: 100% !important; min-width: 100%; max-width: 100%; background: #f5f8fa;'>
                    <tbody>
                      <tr>
                        <td align='right' valign='top'>
                          <div style='height: 34px; line-height: 34px; font-size: 32px;'>&nbsp;</div>
                          <table cellpadding='0' cellspacing='0' border='0' width='88%' style='width: 88% !important; min-width: 88%; max-width: 88%;'>
                            <tbody>
                                <font face=''Source Sans Pro', sans-serif' color='#1a1a1a' style='font-size:
                                  17px; line-height: 20px;'>
                        <span style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px;'><a href='mailto:montupbnusc@gmail.com' style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;'>montupbnusc@gmail.com</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' target='_blank' style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;'>+55 (47)99964-2258</a> &nbsp;&nbsp;|&nbsp;&nbsp; <a href='#' target='_blank' style='font-family: 'Source Sans Pro', Arial, Tahoma, Geneva, sans-serif; color: #1a1a1a; font-size: 17px; line-height: 20px; text-decoration: none;'>Blumenau-SC</a></span>
                     </font> 

                     <div style='height: 35px; line-height: 35px; font-size: 33px;'>&nbsp;</div>

                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
                <td class='mob_pad' width='25' style='width: 25px; max-width: 25px; min-width: 25px;'>&nbsp;</td>
              </tr>
            </table>
    </td>
  </tr>
</table>

";

                    using (var smtp = new SmtpClient("smtp.gmail.com"))
                    {
                        smtp.Port = 587;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential("montupbnusc@gmail.com", "MontUp123321");

                        smtp.EnableSsl = true;
                       await smtp.SendMailAsync(mail);
                    }
                }
                else
                {
                    usuarioDto.ConfirmarEmail = true;
                }

                var result = await _userManager.CreateAsync(user, usuarioDto.Password);
                var userToReturn = _mapper.Map<UsuarioDto>(user);

                if (result.Succeeded)
                    return Created("GetUser", userToReturn);

                return BadRequest(result.Errors);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Há alguma falha {ex.Message}");
            }
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(UsuarioLoginDto usuarioLogin)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(usuarioLogin.Email);

                var result = await _signInManager.CheckPasswordSignInAsync(user, usuarioLogin.Password, false);

                if (result.Succeeded)
                {
                    var appUser = await _userManager.Users
                        .FirstOrDefaultAsync(u => u.NormalizedEmail == usuarioLogin.Email.ToUpper());

                    var userToReturn = _mapper.Map<UsuarioLoginDto>(appUser);

                    return Ok(new
                    {
                        token = GenerateJWToken(appUser).Result,
                        user = userToReturn
                    });
                }

                return Unauthorized();
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Há alguma falha {ex.Message}");
            }
        }

        private async Task<string> GenerateJWToken(Usuario usuario)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, usuario.Id.ToString()),
                new Claim(ClaimTypes.Email, usuario.Email),
                new Claim(ClaimTypes.Name, usuario.UserName)
            };

            var roles = await _userManager.GetRolesAsync(usuario);

            foreach (var role in roles)
                claims.Add(new Claim(ClaimTypes.Role, role));


            var key = new SymmetricSecurityKey(Encoding.ASCII
                .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}