﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/cidade")]
    [ApiController]
    public class CidadeController : Controller
    {
        private readonly IBaseReadRepository _baseReadRepository;
        private readonly IBaseRepository _baseRepository;

        public CidadeController(IBaseReadRepository baseReadRepository, IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
            _baseReadRepository = baseReadRepository;
        }

        [HttpPost, Route("cadastro")]
        public JsonResult Cadastro(Cidade cidade)
        {
            return Json(_baseRepository.Add<Cidade>(cidade));
        }

        [HttpPut, Route("alterar")]
        public JsonResult Alterar(Cidade cidade)
        {
            return Json(_baseRepository.Update<Cidade>(cidade));
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            return Json(_baseRepository.Delete<Cidade>(id));
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos()
        {
            return Json(_baseReadRepository.ObterTodos<Cidade>());
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var cidade = _baseReadRepository.ObterPeloId<Cidade>(id);

            if(cidade == null)
            {
                return NotFound();
            }

            return Json(cidade);
        }

    }
}