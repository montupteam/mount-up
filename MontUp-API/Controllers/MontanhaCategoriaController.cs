﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.ViewModel;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/montanhacategoria")]
    [ApiController]
    public class MontanhaCategoriaController : Controller
    {
        private readonly IMontanhaCategoriaRepository _montanhaCategoriaRepository;
        public MontanhaCategoriaController(IMontanhaCategoriaRepository montanhaCategoriaRepository)
        {
            _montanhaCategoriaRepository = montanhaCategoriaRepository;
        }

        [HttpPost, Route("relacionarvarios")]
        public JsonResult RelacionarVarios(MontanhaCategoriaViewModel montanhaCategoriaViewModel)
        {
            List<int> id = _montanhaCategoriaRepository.RelacionarVarios(montanhaCategoriaViewModel);
            return Json(id);
        }

        [HttpPost, Route("relacionar")]
        public JsonResult Relacionar(MontanhaCategoria montanhaCategoria)
        {
            int id = _montanhaCategoriaRepository.Relacionar(montanhaCategoria);
            return Json(id);
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodosPeloIdMontanha(int idMontanha)
        {
            return Json(_montanhaCategoriaRepository.ObterTodosPeloIdMontanha(idMontanha));
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var montanhaCategoria = _montanhaCategoriaRepository.ObterPeloId(id);
            if (montanhaCategoria == null)
            {
                return NotFound();
            }
            return Json(montanhaCategoria);
        }
    }
}