﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Model;
using Model.ViewModel;
using Repository.Interface;
using Repository.Repositories;

namespace MontUp_API.Controllers
{
    [Route("api/imagemmontanha")]
    [ApiController]
    public class ImagemMontanhaController : Controller
    {
        private readonly string _nomePasta;
        private readonly string _caminho;
        private IImagemMontanhaRepository _imagemMontanhaRepository;
        public ImagemMontanhaController(IImagemMontanhaRepository imagemMontanhaRepository, IHostingEnvironment env)
        {
            _imagemMontanhaRepository = imagemMontanhaRepository;

            string wwwroot = env.WebRootPath;
            _nomePasta = "arquivos-imagemMontanha";
            _caminho = Path.Combine(wwwroot, _nomePasta);

            if (!Directory.Exists(_caminho))
            {
                Directory.CreateDirectory(_caminho);
            }
        }

        [HttpPost, Route("cadastro")]
        public ActionResult Upload([FromForm] ImagemMontanhaViewModel imagemMontanhaViewModel)
        {
            IFormFile imagemArquivo = imagemMontanhaViewModel.Imagem;

            var nomeArquivo = imagemArquivo.FileName;
            var nomeHash = ObterHashDoNomeDoArquivo(nomeArquivo);

            ImagemMontanha imagemMontanha = new ImagemMontanha
            {
                IdMontanha = imagemMontanhaViewModel.MontanhaID
            };

            var caminhoArquivo = Path.Combine(_caminho, nomeHash);
            using (var stream = new FileStream(caminhoArquivo, FileMode.Create))
            {
                imagemArquivo.CopyTo(stream);

                imagemMontanha.UrlImagem = nomeArquivo;
                imagemMontanha.UrlImagemHash = nomeHash;

                _imagemMontanhaRepository.Add(imagemMontanha);
            }
            return Json(imagemArquivo);
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _imagemMontanhaRepository.Delete(id);
            return Json(new { status = apagou });
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var imagemMontanha = _imagemMontanhaRepository.ObterPeloId(id);

            if (imagemMontanha == null)
            {
                return NotFound();
            }

            return File(new FileStream(Path.Combine(_caminho, imagemMontanha.UrlImagemHash), FileMode.Open), "image/jpeg");
        }

        [AllowAnonymous]
        [HttpGet, Route("obterimagem")]
        public ActionResult ObterImagem(int montanhaid)
        {
            var imagemMontanha = _imagemMontanhaRepository.ObterPeloIdMontanha(montanhaid);
        
            if (imagemMontanha == null)
            {
                return NotFound();
            }

            return File(new FileStream(Path.Combine(_caminho, imagemMontanha.UrlImagemHash), FileMode.Open), "image/jpeg");
        }

        [AllowAnonymous]
        [HttpGet, Route("obtertodasimagens")]
        public ActionResult ObterTodasImagens(int id)
        {
            var imagemMontanha = _imagemMontanhaRepository.ObterTodosPeloIdMontanha(id);
            if (imagemMontanha == null)
            {
                return NotFound();
            }
            return File(new FileStream(Path.Combine(_caminho, imagemMontanha[0].UrlImagemHash), FileMode.Open), "image/jpeg");
        }

        public static string ObterHashDoNomeDoArquivo(string nome)
        {
            FileInfo info = new FileInfo(nome);

            var crypt = new SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(info.Name.Replace(info.Extension, "") + DateTime.Now));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return (hash + info.Extension).ToUpper();
        }
    }
}
