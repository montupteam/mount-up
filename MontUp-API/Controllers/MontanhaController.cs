﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/montanha")]
    [ApiController]
    public class MontanhaController : Controller
    {
        private readonly string _nomePasta;
        private readonly string _caminho;
        private readonly IMontanhaRepository _montanhaRepository;
        private readonly IImagemMontanhaRepository _montanhaImagemRepository;

        public MontanhaController(IMontanhaRepository montanhaRepository, IImagemMontanhaRepository montanhaImagemRepository, IHostingEnvironment env)
        {
            _montanhaRepository = montanhaRepository;
            _montanhaImagemRepository = montanhaImagemRepository;

            string wwwroot = env.WebRootPath;
            _nomePasta = "arquivos-imagemMontanha";
            _caminho = Path.Combine(wwwroot, _nomePasta);

            if (!Directory.Exists(_caminho))
            {
                Directory.CreateDirectory(_caminho);
            }
        }

        [HttpPost, Route("cadastro")]
        public JsonResult Cadastro(Montanha montanha)
        {
            _montanhaRepository.Add(montanha);
            return Json(montanha);
        }

        [HttpPut, Route("alterar")]
        public JsonResult Alterar(Montanha montanha)
        {
            bool alterou = _montanhaRepository.Update(montanha);
            return Json(new { status = alterou});
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _montanhaRepository.Delete(id);
            return Json(new { status = apagou });
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos(int idCidade = -1, int idCategoria = -1, int menorTempo = 0, int maiorTempo = 0, string busca = "")
        {
            List<Montanha> montanhas = _montanhaRepository.ObterTodos(idCidade, idCategoria, menorTempo, maiorTempo, busca);

            return Json(montanhas);
        }

        [HttpGet, Route("obtertodasimagens")]
        public JsonResult ObterTodasImagens(int idMontanha = -1)
        {
            List<Montanha> montanhas = _montanhaRepository.ObterTodasImagens(idMontanha);

            return Json(montanhas);
        }
        
        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var montanha = _montanhaRepository.ObterPeloId(id);

            if(montanha == null)
            {
                return NotFound();
            }

            return Json(montanha);
        }
    }
}