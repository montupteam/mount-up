﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/mensagem")]
    [ApiController]
    public class MensagemController : Controller
    {
        private readonly IMensagemRepository _mensagemRepository;
        public MensagemController(IMensagemRepository mensagemRepository)
        {
            _mensagemRepository = mensagemRepository;
        }

        [HttpPost, Route("cadastro")]
        public JsonResult Inserir(Mensagem mensagem)
        {
            mensagem.RegistroAtivo = true;
            _mensagemRepository.Add(mensagem);
            return Json(mensagem);
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _mensagemRepository.Delete(id);
            return Json(new { status = apagou });
        }

        [HttpPut, Route("alterar")]
        public JsonResult Alterar(Mensagem mensagem)
        {
            bool alterou = _mensagemRepository.Update(mensagem);
            return Json(new { status = alterou });
        }

        [HttpGet, Route("obtertodospeloiddochat")]
        public ActionResult ObterTodosPeloIdDoChat(int idChat)
        {
            List<Mensagem> mensagens = _mensagemRepository.ObterTodosPeloIdDoChat(idChat);

            if (mensagens == null)
                return NotFound();

            return Json(mensagens);
        }
    }
}