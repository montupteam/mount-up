﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.ViewModel;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/guia")]
    [Authorize]
    [ApiController]
    public class GuiaController : Controller
    {
        private readonly string _nomePasta;
        private readonly string _caminho;
        private readonly string _nomePasta2;
        private readonly string _caminho2;
        private readonly IGuiaRepository _guiaRepository;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly UserManager<Usuario> _userManager;

        public GuiaController(IGuiaRepository guiaRepository, IUsuarioRepository usuarioRepository, IHostingEnvironment env, UserManager<Usuario> userManager)
        {
            _guiaRepository = guiaRepository;
            _usuarioRepository = usuarioRepository;
            _userManager = userManager;

            string wwwroot = env.WebRootPath;
            _nomePasta = "arquivos-certificados";
            _caminho = Path.Combine(wwwroot, _nomePasta);

            _nomePasta2 = "arquivos-usuario";
            _caminho2 = Path.Combine(wwwroot, _nomePasta2);

            if (!Directory.Exists(_caminho))
            {
                Directory.CreateDirectory(_caminho);
            }
        }

        [HttpPost, Route("cadastro")]
        public async Task<ActionResult> Cadastro([FromForm] GuiaViewModel model)
        {
            Guia guia = new Guia()
            {
                IdCidade = model.IdCidade,
                IdMontanha = model.IdMontanha,
                Instagram = model.Instagram,
                Telefone = model.Telefone,
                Ocupacao = model.Ocupacao,
                CodigoPostal = model.CodigoPostal,
                TemCertificado = model.TemCertificado,
                DescricaoGuia = model.DescricaoGuia,
                DescricaoExperiencia = model.DescricaoExperiencia,
            };

            guia.IdUsuario = (await _userManager.GetUserAsync(User)).Id;

            if(guia.Certificado != null)
            {
                var nomeArquivo = model.Certificado.FileName;
                var nomeHash = ObterHashDoNomeDoArquivo(nomeArquivo);
                model.CertificadoHash = nomeHash;

                var caminhoArquivo = Path.Combine(_caminho, nomeHash);
                using (var stream = new FileStream(caminhoArquivo, FileMode.Create))
                {
                    model.Certificado.CopyTo(stream);
                    guia.Certificado = nomeArquivo;
                    guia.CertificadoHash = nomeHash;
                }
            }
        
            _guiaRepository.Add(guia);
            
            return Json(guia);
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _guiaRepository.Delete(id);
            return Json(new { status = apagou });
        }

        [HttpPost, Route("alterar")]
        public async Task<ActionResult> Alterar(GuiaAlterarViewModel guiaViewModel)
        {
            var idUsuario = (await _userManager.GetUserAsync(User)).Id;
            var guia = _guiaRepository.ObterPeloIdUsuario(idUsuario);

            guia.IdMontanha = guiaViewModel.IdMontanha;
            guia.IdCidade = guiaViewModel.IdCidade;
            guia.Instagram = guiaViewModel.Instagram;
            guia.Telefone = guiaViewModel.Telefone;
            guia.Ocupacao = guiaViewModel.Ocupacao;
            guia.CodigoPostal = guiaViewModel.CodigoPostal;
            guia.DescricaoGuia = guiaViewModel.DescricaoGuia;
            guia.DescricaoExperiencia = guiaViewModel.DescricaoExperiencia;

            _guiaRepository.Update(guia);
            return Json(guia);
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var guia = _guiaRepository.ObterPeloId(id);

            if (guia == null)
            {
                return NotFound();
            }

            return Json(guia);
        }

        [AllowAnonymous]
        [HttpGet, Route("obterpeloidusuario")]
        public ActionResult ObterPeloIdUsuario(int idUsuario)
        {
            Usuario imagemUsuario = _usuarioRepository.ObterPeloId(idUsuario);

            if (imagemUsuario == null)
            {
                return NotFound();
            }

            return File(new FileStream(Path.Combine(_caminho2, imagemUsuario.ImagemHash), FileMode.Open), "image/jpeg");
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos(string busca = "", int idCidade = 0, int idMontanha = 0)
        {
            List<Guia> guias = _guiaRepository.ObterTodos(busca, idCidade, idMontanha);
            return Json(guias);
        }

        public static string ObterHashDoNomeDoArquivo(string nome)
        {
            FileInfo info = new FileInfo(nome);

            var crypt = new SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(info.Name.Replace(info.Extension, "") + DateTime.Now));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return (hash + info.Extension).ToUpper();
        }
    }
}