﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Repository.Interface;

namespace MontUp_API.Controllers
{
    [Route("api/comentario")]
    [ApiController]
    public class ComentarioController : Controller
    {
        private IComentarioRepository _comentarioRepository;

        public ComentarioController(IComentarioRepository comentarioRepository)
        {
            _comentarioRepository = comentarioRepository;
        }      

        [HttpPost, Route("cadastro")]
        public JsonResult Cadastro(Comentario comentario)
        {
            _comentarioRepository.Add(comentario);
            return Json(comentario);
        }

        [HttpDelete, Route("apagar")]
        public JsonResult Apagar(int id)
        {
            bool apagou = _comentarioRepository.Delete(id);
            return Json(new { status = apagou});
        }

        [HttpPut, Route("alterar")]
        public JsonResult Alterar(Comentario comentario)
        {
            bool alterou = _comentarioRepository.Update(comentario);
            return Json(new { status = alterou });
        }

        [HttpGet, Route("obtertodos")]
        public JsonResult ObterTodos()
        {
            List<Comentario> comentarios = _comentarioRepository.ObterTodos();
            return Json(comentarios);
        }

        [HttpGet, Route("obterpeloid")]
        public ActionResult ObterPeloId(int id)
        {
            var comentario = _comentarioRepository.ObterPeloId(id);

            if(comentario == null)
            {
                return NotFound();
            }

            return Json(comentario);
        }
    }
}