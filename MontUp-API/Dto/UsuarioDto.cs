﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MontUp_API.Dto
{
    public class UsuarioDto : Base
    {
        public string UserName { get; set; }
        public string Sobrenome { get; set; }
        public string Email { get; set; }
        public bool ConfirmarEmail { get; set; }
        public string Password { get; set; }
    }
}
