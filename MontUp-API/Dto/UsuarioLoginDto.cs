﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MontUp_API.Dto
{
    public class UsuarioLoginDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
