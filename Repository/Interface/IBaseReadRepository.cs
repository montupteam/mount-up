﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IBaseReadRepository
    {
        List<T> ObterTodos<T>() where T : Base;

        T ObterPeloId<T>(int id) where T : Base;
    }
}
