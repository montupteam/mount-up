﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IUsuarioRepository
    {
        int Add(Usuario usuario);
        bool Update(Usuario usuario);
        bool Delete(int id);
        List<Usuario> ObterTodos();
        Usuario ObterPeloId(int id);
    }
}
