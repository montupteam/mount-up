﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IChatRepository
    {
        int Add(Chat chat);

        bool Delete(int id);

        List<Chat> ObterTodos();

        Chat ObterPeloId(int id);
    }
}
