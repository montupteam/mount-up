﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IMensagemRepository
    {
        int Add(Mensagem mensagem);

        bool Update(Mensagem mensagem);

        bool Delete(int id);

        List<Mensagem> ObterTodosPeloIdDoChat(int idChat);        
    }
}
