﻿using Model;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IMontanhaCategoriaRepository
    {
        List<int> RelacionarVarios(MontanhaCategoriaViewModel montanhaCategoriaViewModel);
        int Relacionar(MontanhaCategoria montanhaCategoria);
        List<MontanhaCategoria> ObterTodosPeloIdMontanha(int idMontanha);
        MontanhaCategoria ObterPeloId(int id);
    }
}
