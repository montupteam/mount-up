﻿using Model;
using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IImagemMontanhaRepository
    {
        int Add(ImagemMontanha imagemMontanha);

        bool Update(ImagemMontanha imagemMontanha);

        bool Delete(int id);

        ImagemMontanha ObterPeloId(int id);

        ImagemMontanha ObterPeloIdMontanha(int idMontanha);

        List<ImagemMontanha> ObterTodosPeloIdMontanha(int idMontanha);
    }
}
