﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interface
{
    public interface IBaseRepository
    {
        int Add<T>(T entity) where T : Base;

        bool Update<T>(T entity) where T : Base;

        Task<bool> SaveChangesAsync();

        bool Delete<T>(int id) where T : Base;
    }
}
