﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interface
{
    public interface ILocalRepository
    {
        int Add(Local local);

        bool Update(Local local);

        bool Delete(int id);

        List<Local> ObterTodos();

        Local ObterPeloId(int id);
    }
}
