﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IComentarioRepository
    {
        int Add(Comentario comentario);

        bool Update(Comentario comentario);

        bool Delete(int id);

        List<Comentario> ObterTodos();

        Comentario ObterPeloId(int id);
    }
}
