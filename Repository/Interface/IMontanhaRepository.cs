﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Interface
{
    public interface IMontanhaRepository
    {
        int Add(Montanha montanha);
        bool Update(Montanha montanha);
        bool Delete(int id);
        List<Montanha> ObterTodos(int idCidade, int idCategoria, int menorTempo, int maiorTempo, string busca);
        List<Montanha> ObterTodasImagens(int idMontanha);
        Montanha ObterPeloId(int id);
    }
}
