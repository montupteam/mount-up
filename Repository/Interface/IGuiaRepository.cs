﻿using Microsoft.AspNetCore.Http;
using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interface
{
    public interface IGuiaRepository
    {
        int Add(Guia guia);

        bool Update(Guia guia);

        bool Delete(int id);

        List<Guia> ObterTodos(string busca, int idCidade, int idMontanha);

        Guia ObterPeloId(int id);

        Guia ObterPeloIdUsuario(int idUsuario);
    }
}
