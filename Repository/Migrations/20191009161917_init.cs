﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Repository.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    nome = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    sobrenome = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    cpf = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    data_nascimento = table.Column<DateTime>(type: "DATETIME", nullable: true),
                    email = table.Column<string>(type: "VARCHAR(100)", maxLength: 256, nullable: true),
                    confirmar_email = table.Column<bool>(type: "BIT", nullable: false),
                    senha = table.Column<string>(type: "VARCHAR(350)", nullable: true),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    imagem_usuario = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    imagem_hash = table.Column<string>(type: "VARCHAR(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "categorias",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    nome = table.Column<string>(type: "VARCHAR(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_categorias", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "cidades",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    nome = table.Column<string>(type: "VARCHAR(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cidades", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    UsuarioId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "montanhas",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    nome = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    altitude = table.Column<decimal>(type: "DECIMAL(8,2)", nullable: false),
                    extensao = table.Column<decimal>(type: "DECIMAL(8,2)", nullable: false),
                    duracao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    nivel = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    idade_minima = table.Column<int>(type: "INT", nullable: false),
                    descricao = table.Column<string>(type: "TEXT", nullable: true),
                    latitude = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    longitude = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    id_cidade = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_montanhas", x => x.id);
                    table.ForeignKey(
                        name: "FK_montanhas_cidades_id_cidade",
                        column: x => x.id_cidade,
                        principalTable: "cidades",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "chats",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    id_montanha = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_chats", x => x.id);
                    table.ForeignKey(
                        name: "FK_chats_montanhas_id_montanha",
                        column: x => x.id_montanha,
                        principalTable: "montanhas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "comentarios",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    id_usuario = table.Column<int>(type: "INT", nullable: false),
                    id_montanha = table.Column<int>(type: "INT", nullable: false),
                    comentario_texto = table.Column<string>(type: "TEXT", nullable: true),
                    data_comentario = table.Column<DateTime>(type: "DATETIME", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_comentarios", x => x.id);
                    table.ForeignKey(
                        name: "FK_comentarios_montanhas_id_montanha",
                        column: x => x.id_montanha,
                        principalTable: "montanhas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_comentarios_AspNetUsers_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "guias",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    id_usuario = table.Column<int>(type: "INT", nullable: false),
                    id_montanha = table.Column<int>(type: "INT", nullable: false),
                    id_cidade = table.Column<int>(type: "INT", nullable: false),
                    instagram = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    telefone = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    ocupacao = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    codigo_postal = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    certificado = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    certificado_hash = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    tem_certificado = table.Column<bool>(type: "BIT", nullable: false),
                    descricao_guia = table.Column<string>(type: "TEXT", nullable: true),
                    descricao_experiencia = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_guias", x => x.id);
                    table.ForeignKey(
                        name: "FK_guias_cidades_id_cidade",
                        column: x => x.id_cidade,
                        principalTable: "cidades",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_guias_montanhas_id_montanha",
                        column: x => x.id_montanha,
                        principalTable: "montanhas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_guias_AspNetUsers_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "imagens_montanhas",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    id_montanha = table.Column<int>(type: "INT", nullable: false),
                    url_imagem = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    url_imagem_hash = table.Column<string>(type: "VARCHAR(100)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_imagens_montanhas", x => x.id);
                    table.ForeignKey(
                        name: "FK_imagens_montanhas_montanhas_id_montanha",
                        column: x => x.id_montanha,
                        principalTable: "montanhas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "locais",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    id_cidade = table.Column<int>(type: "INT", nullable: false),
                    id_montanha = table.Column<int>(type: "INT", nullable: false),
                    nome = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    descricao = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_locais", x => x.id);
                    table.ForeignKey(
                        name: "FK_locais_cidades_id_cidade",
                        column: x => x.id_cidade,
                        principalTable: "cidades",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_locais_montanhas_id_montanha",
                        column: x => x.id_montanha,
                        principalTable: "montanhas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "montanhas_categorias",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    id_categoria = table.Column<int>(type: "INT", nullable: false),
                    id_montanha = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_montanhas_categorias", x => x.id);
                    table.ForeignKey(
                        name: "FK_montanhas_categorias_categorias_id_categoria",
                        column: x => x.id_categoria,
                        principalTable: "categorias",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_montanhas_categorias_montanhas_id_montanha",
                        column: x => x.id_montanha,
                        principalTable: "montanhas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "mensagens",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    registro_ativo = table.Column<bool>(type: "BIT", nullable: false),
                    data_criacao = table.Column<DateTime>(type: "DATETIME", nullable: false),
                    id_chat = table.Column<int>(type: "INT", nullable: false),
                    id_usuario = table.Column<int>(type: "INT", nullable: true),
                    id_guia = table.Column<int>(type: "INT", nullable: true),
                    texto = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mensagens", x => x.id);
                    table.ForeignKey(
                        name: "FK_mensagens_chats_id_chat",
                        column: x => x.id_chat,
                        principalTable: "chats",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mensagens_guias_id_guia",
                        column: x => x.id_guia,
                        principalTable: "guias",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_mensagens_AspNetUsers_id_usuario",
                        column: x => x.id_usuario,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "confirmar_email", "cpf", "data_criacao", "data_nascimento", "email", "EmailConfirmed", "imagem_hash", "imagem_usuario", "LockoutEnabled", "LockoutEnd", "nome", "NormalizedEmail", "NormalizedUserName", "senha", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "registro_ativo", "SecurityStamp", "sobrenome", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 5, 0, "3b49b908-94e5-40c4-ad14-03185e688858", false, null, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(2625), null, "matheus@gmail.com", false, null, null, true, null, "Matheus", "MATHEUS@GMAIL.COM", "MATHEUS", "a2bc101d9c97208c39cb0193aeab92849d480e8e4bacf2bb92a76fdbda4e81102718d6949dddfebdeaa8adde3c66462127f5fd49c882cac73ec2441c77519d89", "AQAAAAEAACcQAAAAEIqYHm5wMYV9Q12A2XuPWbiiNpMo+MzEqkstXg6KY1ock/P+PWTe0niln9TMhpTGHQ==", null, false, true, "DMIGEGEOFASNPKKAI53CX7YH2GTGMLXU", "Danato", false, "Matheus" },
                    { 3, 0, "371e6bf6-2787-4135-a518-ec5e9ab394e6", false, null, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(2601), null, "nathanalves001@gmail.com", false, null, null, true, null, "Nathan", "NATHANALVES001@GMAIL.COM", "NATHAN", "0a47151a074e633ab7b6bed6aab724abbddcd3250f80a06bc612a233a907805101f2441b5b2926e54ce8ac8cfbc074bb7a56748830487df09591dbe167e800f6", "AQAAAAEAACcQAAAAEDasw8SklpHTWK1/2tAU/kXENWbNGtFRCSNX6+dilI0lfXiKQMXNlNetPnx1uVTr5g==", null, false, true, "RKYYTWOWCTGQXECW4FCHGLOAAOTWE6RA", "Micael", false, "Nathan" },
                    { 2, 0, "47c8fde7-f39b-4677-b97a-b96b05a92e50", false, null, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(2581), null, "Gustavo@gmail.com", false, null, null, true, null, "Gustavo", "GUSTAVO@GMAIL.COM", "GUSTAVO", "dc199c20e841df931a283634e6d8fa19ef7ef4988acd55b7cb0b938b8e334c8b073b6e2a597d4930f75b028960655df5852b6b1601f56bfe5eb31beccf06788d", "AQAAAAEAACcQAAAAEEhdQqr/qBuj0o0mbBaf/Kdol63nbfWJOzKO7mxra50WzWcvIetByNeR5dS7VyhmJQ==", null, false, true, "ZH3JKPCMZQD3KRA3TJU5FPR7BUBXBZKT", "Manoel", false, "Gustavo" },
                    { 1, 0, "15b4e749-47f0-449e-a194-27d7c7fcdb62", false, null, new DateTime(2019, 10, 9, 13, 19, 17, 420, DateTimeKind.Local).AddTicks(4338), null, "andre@gmail.com", false, null, null, true, null, "Andre", "ANDRE@GMAIL.COM", "ANDRE", "e1223d9bbcd82236f9f09ae1f5578e3cbbd4e8f48954cead3003be60ac85629726dc04b1f875353459f97ba4a4283a1a6adb89d3524bb4816c7125964097106c", "AQAAAAEAACcQAAAAEMCnl8w9nsLU3srK79HqtaA3mOq4ZKyJrfcLqV1370URklz5P9xTJ9LjG0dh5nYDcg==", null, false, true, "PZ2DX2TZJUKNQUVLSJEUKSV6CLEO6K2K", "Hinckel", false, "Andre" },
                    { 4, 0, "bd2957a9-09cb-432c-a9e1-eaff0ab35e2a", false, null, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(2608), null, "eduardo@gmail.com", false, null, null, true, null, "Eduardo", "EDUARDO@GMAIL.COM", "EDUARDO", "bdb36a0a12e22a77ffe7ab04d03c6fe209c8dce452eab3d58733b3f5e7133baf92202a9b78619fdd5e24db0ad11fde37210be53782d33c8072fbc907c5ade8ff", "AQAAAAEAACcQAAAAECcvAzTb3A6UunmjOJA0qzGf6uX9WhiZVUBSxaw5vqpfpig5KCOTOD1bePExFkgBtg==", null, false, true, "XK6PFK7W2ZPUONSDJOZBVTXYPUFHPQDT", "Gabriel", false, "Eduardo" }
                });

            migrationBuilder.InsertData(
                table: "categorias",
                columns: new[] { "id", "data_criacao", "nome", "registro_ativo" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(4265), "Trilha", true },
                    { 2, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(4288), "Morro", true },
                    { 3, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(4291), "Escalada", true },
                    { 4, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(4294), "Rapel", true }
                });

            migrationBuilder.InsertData(
                table: "cidades",
                columns: new[] { "id", "data_criacao", "nome", "registro_ativo" },
                values: new object[,]
                {
                    { 3, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7039), "Agrolândia", true },
                    { 204, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7644), "Ponte Alta do Norte", true },
                    { 203, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7641), "Ponte Alta", true },
                    { 202, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7638), "Pomerode", true },
                    { 201, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7636), "Planalto Alegre", true },
                    { 200, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7632), "Piratuba", true },
                    { 199, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7629), "Pinheiro Preto", true },
                    { 198, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7627), "Pinhalzinho", true },
                    { 196, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7621), "Pescaria Brava", true },
                    { 205, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7646), "Ponte Serrada", true },
                    { 195, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7619), "Peritiba", true },
                    { 194, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7616), "Penha", true },
                    { 193, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7613), "Pedras Grandes", true },
                    { 192, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7610), "Paulo Lopes", true },
                    { 191, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7608), "Passos Maia", true },
                    { 197, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7624), "Petrolândia", true },
                    { 206, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7649), "Porto Belo", true },
                    { 208, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7655), "Pouso Redondo", true },
                    { 190, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7605), "Passo de Torres", true },
                    { 222, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7692), "Rio Negrinho", true },
                    { 221, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7690), "Rio Fortuna", true },
                    { 220, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7687), "Rio dos Cedros", true },
                    { 219, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7684), "Rio do Sul", true },
                    { 218, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7682), "Rio do Oeste", true },
                    { 217, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7679), "Rio do Campo", true },
                    { 207, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7652), "Porto União", true },
                    { 216, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7676), "Rio das Antas", true },
                    { 214, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7671), "Quilombo", true },
                    { 213, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7668), "Princesa", true },
                    { 212, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7665), "Presidente Nereu", true },
                    { 211, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7663), "Presidente Getúlio", true },
                    { 210, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7660), "Presidente Castello Branco", true },
                    { 209, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7657), "Praia Grande", true },
                    { 215, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7673), "Rancho Queimado", true },
                    { 189, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7602), "Paraíso", true },
                    { 187, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7597), "Palmitos", true },
                    { 223, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7695), "Rio Rufino", true },
                    { 167, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7541), "Monte Carlo", true },
                    { 166, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7538), "Mondaí", true },
                    { 165, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7536), "Modelo", true },
                    { 164, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7533), "Mirim Doce", true },
                    { 163, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7530), "Meleiro", true },
                    { 162, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7527), "Matos Costa", true },
                    { 168, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7544), "Monte Castelo", true },
                    { 161, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7525), "Massaranduba", true },
                    { 159, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7519), "Maravilha", true },
                    { 158, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7516), "Maracajá", true },
                    { 157, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7514), "Major Vieira", true },
                    { 156, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7511), "Major Gercino", true },
                    { 155, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7508), "Mafra", true },
                    { 154, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7505), "Macieira", true },
                    { 160, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7522), "Marema", true },
                    { 169, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7547), "Morro da Fumaça", true },
                    { 170, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7549), "Morro Grande", true },
                    { 171, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7552), "Navegantes", true },
                    { 186, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7594), "Palmeira", true },
                    { 185, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7591), "Palma Sola", true },
                    { 184, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7589), "Palhoça", true },
                    { 183, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7586), "Painel", true },
                    { 182, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7583), "", true },
                    { 181, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7580), "Paial", true },
                    { 180, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7577), "Ouro Verde", true },
                    { 179, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7575), "Ouro", true },
                    { 178, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7572), "Otacílio Costa", true },
                    { 177, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7569), "Orleans", true },
                    { 176, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7566), "Novo Horizonte", true },
                    { 175, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7563), "Nova Veneza", true },
                    { 174, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7560), "Nova Trento", true },
                    { 173, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7558), "Nova Itaberaba", true },
                    { 172, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7555), "Nova Erechim", true },
                    { 188, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7600), "Papanduva", true },
                    { 224, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7698), "Riqueza", true },
                    { 226, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7704), "Romelândia", true },
                    { 153, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7503), "Luzerna", true },
                    { 277, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7850), "Treze Tílias", true },
                    { 276, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7847), "Treze de Maio", true },
                    { 275, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7844), "Treviso", true },
                    { 274, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7841), "Três Barras", true },
                    { 273, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7835), "Timbó Grande", true },
                    { 272, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7830), "Timbó", true },
                    { 278, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7853), "Trombudo Central", true },
                    { 271, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7828), "Timbé do Sul", true },
                    { 269, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7822), "Tigrinhos", true },
                    { 268, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7819), "Tangará", true },
                    { 267, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7817), "Taió", true },
                    { 266, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7814), "Sul Brasil", true },
                    { 265, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7811), "Sombrio", true },
                    { 264, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7809), "Siderópolis", true },
                    { 270, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7825), "Tijucas", true },
                    { 279, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7856), "Tubarão", true },
                    { 280, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7858), "Tunápolis", true },
                    { 281, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7861), "Turvo", true },
                    { 296, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7903), "Zortéa", true },
                    { 295, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7899), "Xaxim", true },
                    { 294, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7897), "Xavantina", true },
                    { 293, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7894), "Xanxerê", true },
                    { 292, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7891), "Witmarsum", true },
                    { 291, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7889), "Vitor Meireles", true },
                    { 290, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7886), "Videira", true },
                    { 289, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7883), "Vidal Ramos", true },
                    { 288, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7881), "Vargem Bonita", true },
                    { 287, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7878), "Vargem", true },
                    { 286, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7875), "Vargeão", true },
                    { 285, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7873), "Urussanga", true },
                    { 284, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7870), "Urupema", true },
                    { 283, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7867), "Urubici", true },
                    { 282, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7864), "União do Oeste", true },
                    { 263, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7806), "Serra Alta", true },
                    { 225, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7701), "Rodeio", true },
                    { 262, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7803), "Seara", true },
                    { 260, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7798), "Saudades", true },
                    { 240, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7742), "São Bernardino", true },
                    { 239, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7739), "São Bento do Sul", true },
                    { 238, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7737), "Santo Amaro da Imperatriz", true },
                    { 237, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7734), "Santiago do Sul", true },
                    { 236, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7731), "Santa Terezinha do Progresso", true },
                    { 235, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7728), "Santa Terezinha", true },
                    { 241, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7745), "São Bonifácio", true },
                    { 234, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7726), "Santa Rosa do Sul", true },
                    { 232, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7720), "Santa Helena", true },
                    { 231, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7718), "Santa Cecília", true },
                    { 230, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7715), "Sangão", true },
                    { 229, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7712), "Salto Veloso", true },
                    { 228, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7709), "Saltinho", true },
                    { 227, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7707), "Salete", true },
                    { 233, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7723), "Santa Rosa de Lima", true },
                    { 242, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7747), "São Carlos", true },
                    { 243, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7750), "São Cristóvão do Sul", true },
                    { 244, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7753), "São Domingos", true },
                    { 259, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7795), "São Pedro de Alcântara", true },
                    { 258, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7792), "São Miguel do Oeste", true },
                    { 257, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7790), "São Miguel da Boa Vista", true },
                    { 256, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7787), "São Martinho", true },
                    { 255, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7784), "São Ludgero", true },
                    { 254, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7781), "São Lourenço do Oeste", true },
                    { 253, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7778), "São José do Cerrito", true },
                    { 252, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7775), "São José do Cedro", true },
                    { 251, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7772), "São José", true },
                    { 250, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7769), "São Joaquim", true },
                    { 249, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7767), "São João do Sul", true },
                    { 248, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7764), "São João do Oeste", true },
                    { 247, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7761), "São João do Itaperiú", true },
                    { 246, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7758), "São João Batista", true },
                    { 245, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7756), "São Francisco do Sul", true },
                    { 261, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7801), "Schroeder", true },
                    { 2, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7026), "Abelardo Luz", true },
                    { 152, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7499), "Luiz Alves", true },
                    { 150, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7494), "Lindóia do Sul", true },
                    { 55, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7186), "Campo Alegre", true },
                    { 54, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7183), "Camboriú", true },
                    { 53, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7180), "Calmon", true },
                    { 52, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7178), "Caibi", true },
                    { 51, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7175), "Caçador", true },
                    { 50, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7172), "Brusque", true },
                    { 56, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7190), "Campo Belo do Sul", true },
                    { 49, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7169), "Brunópolis", true },
                    { 47, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7164), "Braço do Norte", true },
                    { 46, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7161), "Botuverá", true },
                    { 45, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7158), "Bombinhas", true },
                    { 44, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7156), "Bom Retiro", true },
                    { 43, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7153), "Bom Jesus do Oeste", true },
                    { 42, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7150), "Bom Jesus", true },
                    { 48, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7167), "Braço do Trombudo", true },
                    { 57, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7193), "Campo Erê", true },
                    { 58, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7196), "Campos Novos", true },
                    { 59, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7199), "Canelinha", true },
                    { 74, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7241), "Coronel Martins", true },
                    { 73, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7238), "Coronel Freitas", true },
                    { 72, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7235), "Cordilheira Alta", true },
                    { 71, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7233), "Concórdia", true },
                    { 70, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7230), "Cocal do Sul", true },
                    { 69, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7227), "Chapecó", true },
                    { 68, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7224), "Chapadão do Lageado", true },
                    { 67, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7221), "Cerro Negro", true },
                    { 66, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7218), "Celso Ramos", true },
                    { 65, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7216), "Caxambu do Sul", true },
                    { 64, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7213), "Catanduvas", true },
                    { 63, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7210), "Capivari de Baixo", true },
                    { 62, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7207), "Capinzal", true },
                    { 61, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7205), "Capão Alto", true },
                    { 60, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7202), "Canoinhas", true },
                    { 41, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7147), "Bom Jardim da Serra", true },
                    { 40, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7145), "Bocaina do Sul", true },
                    { 39, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7142), "Blumenau", true },
                    { 38, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7139), "Biguaçu", true },
                    { 18, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7083), "Araquari", true },
                    { 17, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7080), "Arabutã", true },
                    { 16, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7078), "Apiúna", true },
                    { 15, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7075), "Antônio Carlos", true },
                    { 14, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7072), "Anitápolis", true },
                    { 13, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7070), "Anita Garibaldi", true },
                    { 12, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7067), "Angelina", true },
                    { 11, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7064), "Anchieta", true },
                    { 10, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7061), "Alto Bela Vista", true },
                    { 9, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7058), "Alfredo Wagner", true },
                    { 8, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7053), "Águas Mornas", true },
                    { 7, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7050), "Águas Frias", true },
                    { 6, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7047), "Águas de Chapecó", true },
                    { 5, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7045), "Água Doce", true },
                    { 4, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7042), "Agronômica", true },
                    { 19, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7086), "Araranguá", true },
                    { 75, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7245), "Correia Pinto", true },
                    { 20, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7089), "Armazém", true },
                    { 22, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7094), "Arvoredo", true },
                    { 37, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7136), "Benedito Novo", true },
                    { 36, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7134), "Belmonte", true },
                    { 35, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7131), "Bela Vista do Toldo", true },
                    { 34, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7128), "Barra Velha", true },
                    { 33, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7126), "Barra Bonita", true },
                    { 32, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7122), "Bandeirante", true },
                    { 31, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7119), "Balneário Rincão", true },
                    { 30, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7116), "Balneário Piçarras", true },
                    { 29, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7113), "Balneário Gaivota", true },
                    { 28, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7110), "Balneário Camboriú", true },
                    { 27, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7108), "Balneário Barra do Sul", true },
                    { 26, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7105), "Balneário Arroio do Silva", true },
                    { 25, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7102), "Aurora", true },
                    { 24, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7100), "Atalanta", true },
                    { 23, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7097), "Ascurra", true },
                    { 21, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7092), "Arroio Trinta", true },
                    { 151, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7496), "Lontras", true },
                    { 76, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7248), "Corupá", true },
                    { 78, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7253), "Cunha Porã", true },
                    { 130, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7438), "Itapiranga", true },
                    { 129, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7436), "Itapema", true },
                    { 128, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7432), "Itajaí", true },
                    { 127, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7429), "Itaiópolis", true },
                    { 126, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7427), "Itá", true },
                    { 125, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7424), "Irineópolis", true },
                    { 131, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7441), "Itapoá", true },
                    { 124, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7421), "Irati", true },
                    { 122, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7416), "Iraceminha", true },
                    { 121, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7413), "Ipumirim", true },
                    { 120, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7410), "Ipuaçu", true },
                    { 119, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7406), "Iporã do Oeste", true },
                    { 118, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7404), "Ipira", true },
                    { 117, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7401), "Iomerê", true },
                    { 123, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7418), "Irani", true },
                    { 132, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7444), "Ituporanga", true },
                    { 133, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7447), "Jaborá", true },
                    { 134, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7450), "Jacinto Machado", true },
                    { 1, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(6554), "Abdon Batista", true },
                    { 148, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7488), "Lebon Régis", true },
                    { 147, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7485), "Lauro Müller", true },
                    { 146, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7483), "Laurentino", true },
                    { 145, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7480), "Lajeado Grande", true },
                    { 144, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7477), "Laguna", true },
                    { 143, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7474), "Lages", true },
                    { 142, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7471), "Lacerdópolis", true },
                    { 141, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7469), "Jupiá", true },
                    { 140, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7466), "José Boiteux", true },
                    { 139, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7463), "Joinville", true },
                    { 138, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7460), "Joaçaba", true },
                    { 137, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7458), "Jardinópolis", true },
                    { 136, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7455), "Jaraguá do Sul", true },
                    { 135, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7452), "Jaguaruna", true },
                    { 116, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7398), "Indaial", true },
                    { 115, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7396), "Imbuia", true },
                    { 114, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7393), "Imbituba", true },
                    { 113, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7390), "Imaruí", true },
                    { 93, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7295), "Fraiburgo", true },
                    { 92, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7292), "Forquilhinha", true },
                    { 91, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7290), "Formosa do Sul", true },
                    { 90, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7287), "Florianópolis", true },
                    { 89, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7284), "Flor do Sertão", true },
                    { 88, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7281), "Faxinal dos Guedes", true },
                    { 87, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7279), "Erval Velho", true },
                    { 86, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7276), "Ermo", true },
                    { 85, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7273), "Entre Rios", true },
                    { 84, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7270), "Doutor Pedrinho", true },
                    { 83, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7268), "Dona Emma", true },
                    { 82, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7265), "Dionísio Cerqueira", true },
                    { 81, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7262), "Descanso", true },
                    { 80, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7260), "Curitibanos", true },
                    { 79, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7256), "Cunhataí", true },
                    { 94, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7298), "Frei Rogério", true },
                    { 77, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7251), "Criciúma", true },
                    { 95, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7300), "Galvão", true },
                    { 97, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7306), "Garuva", true },
                    { 112, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7387), "Ilhota", true },
                    { 111, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7385), "Içara", true },
                    { 110, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7382), "Ibirama", true },
                    { 109, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7379), "Ibicaré", true },
                    { 108, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7377), "Ibiam", true },
                    { 107, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7374), "Herval d'Oeste", true },
                    { 106, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7371), "Guatambu", true },
                    { 105, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7368), "Guarujá do Sul", true },
                    { 104, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7365), "Guaramirim", true },
                    { 103, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7323), "Guaraciaba", true },
                    { 102, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7320), "Guabiruba", true },
                    { 101, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7317), "Gravatal", true },
                    { 100, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7314), "Grão-Pará", true },
                    { 99, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7311), "Governador Celso Ramos", true },
                    { 98, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7309), "Gaspar", true },
                    { 96, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7303), "Garopaba", true },
                    { 149, new DateTime(2019, 10, 9, 13, 19, 17, 422, DateTimeKind.Local).AddTicks(7491), "Leoberto Leal", true }
                });

            migrationBuilder.InsertData(
                table: "montanhas",
                columns: new[] { "id", "altitude", "data_criacao", "descricao", "duracao", "extensao", "id_cidade", "idade_minima", "latitude", "longitude", "nivel", "nome", "registro_ativo" },
                values: new object[,]
                {
                    { 4, 820m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3091), "Em meio à natureza tão abundante de Corupá, está um lugar muito procurado por quem estima sentir a imponência da Mata Atlântica. O imponente Morro do Boi tem sua trilha localizada no Ano Bom. “A trilha já consolidada é relativamente bem marcada, podendo estar fechada em alguns pontos, sendo necessário bastante atenção a descida íngreme, principalmente na parte próxima ao cume”, conta o guia Rossano Tribes.", new DateTime(2000, 2, 2, 2, 2, 2, 0, DateTimeKind.Unspecified), 3m, 28, 0, "-26.426844", "-49.230876", "Moderado", "Morro do Boi", true },
                    { 11, 936m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3118), "O morro Spitzkopf (numa tradução literal do alemão cabeça pontuda) é uma montanha no município brasileiro de Blumenau, com 936 metros de altitude ( a altura oficial pelo IBGE é de 913 metros e 98 cm). Situa-se no Parque Ecológico Spitzkopf, cuja área é de mais de 5.000 m² de Mata Atlântica, piscinas naturais, cascatas e riachos. O morro Spitzkopf foi escalado pela primeira vez, em 1872, pelo comandante das Guardas de Batedores do Mato - Friedrich Deeke (oficialmente investido na função com o nome aportuguesado: Frederico Deeke). Em 19 e 20 de julho de 1892 a montanha foi escalada pelos excursionistas Otto Wehmuth, Christian Imroth, Fritz Alfarht e outros. Em 17 de julho de 1929, foi criado o Spitzkopf - Klub, tendo como diretor Otto Huber, secretário Rudolf Hollenweger, cobrador Alfredo Gossweiler, rancheiro, guarda da cabana, Fritz Hasse. Proprietários Paul Scheidemantel, Gauche (alfaiate) e Wünsch. Sobre o Spitzkopf há farta literatura contida na Revista Blumenau em Cadernos, entretanto merece especial referência o fato de haver sido escolhido pelo professor Max Humpl para lá, numa altitude de 750 metros (segundo consta do diário de Max Humpl sua casa situar-se-ia aos 500 metros), quase no topo da montanha de 938 metros, construir sua residência.", new DateTime(2000, 4, 4, 4, 4, 4, 0, DateTimeKind.Unspecified), 6m, 39, 18, "-27.025026", "-49.132220", "Fácil", "Spitzkopf", true },
                    { 12, 718m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3122), "O morro está localizado dentro do Parque das Nascentes, que faz parte do Parque Nacional da Serra do Itajaí (PNSI), o qual compreende uma área de 57.374 hectares e altitudes de 80 a 1039 metros, abrangendo nove municípios de Santa Catarina – Indaial, Blumenau, Botuverá, Gaspar, Vidal Ramos, Apiúna, Guabiruba, Ascurra e Presidente Nereu. Também na sede do parque há a trilha da chuva, com 2,7 km de extensão e diversas travessias de rio. O acesso ao local é controlado pelo Instituto Parque das Nascentes.", new DateTime(2000, 3, 3, 3, 3, 3, 0, DateTimeKind.Unspecified), 4.5m, 39, 18, "-27.069194", "-49.106680", "Fácil/Moderado", "Morro do Sapo", true },
                    { 2, 967m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3081), "O Monte Crista é cercado por uma beleza magnífica e muita história. É uma formação rochosa de puro granito e do alto de seu cume é possível vislumbrar vales extensos com árvores centenárias e natureza intocada. Em suas encostas, passam os rios Três Barras e o Crista, com suas cascatas, lajes de granito e pequenos lagos de águas geladas e cristalinas. Conta que o caminho ao alto já foi utilizado por padres jesuítas há mais de 200 anos, existindo escadarias com mais de mil anos de existência e ainda bem conservadas, há referências ao local como “Caminho dos Ambrósios”, utilizado para cruzar a Serra do Mar desde Curitiba até a Baia da Babitonga – ainda pode ter sido aberto seguindo um ramal do Caminho de Peabiru, que se liga ao Peru.Segundo o guia Rossano Tribes, devido à grande quantidade de pessoas que frequenta o local, a trilha é bem marcada. Mas é preciso preparo físico para encarar a distância e pontos difíceis.", new DateTime(2000, 4, 4, 4, 4, 4, 0, DateTimeKind.Unspecified), 8m, 97, 0, "-26.080870", "-48.919780", "Moderado/Difícil", "Monte Crista", true },
                    { 5, 1350m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3095), "O conjunto de montanhas que compreende o Quiriri possui cerca de 30 cumes, cuja altura varia entre 1.300 a 1.580 metros. O cenário lembra filmes medievais, com uma beleza incomparável. De acordo com o guia Carlos Reinke, a entrada de acesso para é pelas Serra Dona Francisca - o local faz parte de uma Área de Proteção Ambiental, que compreende os municípios de Campo Alegre, Garuva e Joinville. “Até chegar na fazenda de acesso, da 50km de estrada de chão. Para ir para fazenda dos Campos do Quiriri de 4x4 ou fazer trekking precisa de autorização”, comenta. A trilha indicada, comenta Carlos, vai do portão do portão da fazenda até a Pedra da Tartaruga.“É necessário ir com pessoas que já fizeram esse trekking, acompanhados de um GPS pois as condições climáticas mudam rapidamente”, comenta. Quiriri na língua tupi guarani significa “Silêncio Noturno”, e contam os antigos que ali era morada de índios e bugres.", new DateTime(2000, 6, 6, 6, 6, 6, 0, DateTimeKind.Unspecified), 7.8m, 97, 0, "-26.012465", "-48.923789", "Moderado", "Pedra da Tartaruga", true },
                    { 6, 926m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3099), "O Pico, conforme o guia Carlos Reinke, está no maciço conhecido por “Morro Boa Vista”, que ainda possui o Morro do Meio e o Morro das Antenas. O acesso a trilha é pelo bairro Águas Claras, porém, a entrada precisa de uma autorização por passar na fazenda da família Krause. Também é possível chegar ao local a partir do Morro das Antenas, existe uma trilha que atravessa os cumes.", new DateTime(2000, 2, 2, 2, 2, 2, 0, DateTimeKind.Unspecified), 2.7m, 136, 0, "-26.515440", "-49.061781", "Moderado", "Pico Jaraguá", true },
                    { 1, 970m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3031), "É chamado de Castelo dos Bugres uma formação de rochas sobrepostas, no alto da Serra do Mar, que pode ser apreciada a partir da rodovia Dona Francisca. O destino atrai montanhistas interessados em escalada ou rapel, e aventureiros que querem apreciar a vista e desfrutar de uma pequena caverna no local. O percurso é marcado por mananciais de água, rochas e a beleza estonteante da Mata Atlântica, percorrendo as nascentes do rio Piraí. O nível de dificuldade é considerado fácil, pois a trilha é bem marcada e aberta. A dica é evitar os dias de chuva e cuidar com as bifurcações, pois é possível se perder.", new DateTime(2000, 2, 2, 2, 2, 2, 0, DateTimeKind.Unspecified), 4m, 139, 0, "-26.234753", "-49.048240", "Fácil", "Castelo dos Bugres", true },
                    { 7, 1149m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3103), "De acordo com informações do site Terra Média Trekking, o Jurapê é uma das montanhas mais exigentes de Santa Catarina. A trilha é relativamente bem marcada, possuindo inclusive algumas placas indicativas em certas bifurcações, mas ainda assim há trechos que exigem atenção para não se perder. “O trecho final da trilha é bastante íngreme, exigindo bastante preparo físico”, destaca o guia Rossano Tribes. Com a história oficial que no de junho de 1886, o imigrante suíço Johan Paul Schmalz, juntamente com Bruno Clauser, Hahn, Jacob Schmalz, Otto Delitsch e mais dois sujeitos definidos como “dois alugados”, atingiram o ponto culminante da montanha, após três dias abrindo a trilha.", new DateTime(2000, 4, 4, 4, 4, 4, 0, DateTimeKind.Unspecified), 6m, 139, 0, "-26.259292", "-49.008120", "Difícil", "Pico Jurapê", true },
                    { 13, 915m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3126), "Morro do Cambirela localiza-se no município de Palhoça/SC, o morro faz parte de um conjunto de montanhas pertencentes ao Parque Estadual da Serra do Tabuleiro, sendo esse parque a maior unidade de conservação de proteção integral do estado de Santa Catarina – Brasil. O Morro do Cambirela situa-se próximo a BR – 101, uma montanha que eleva-se a um pouco mais de 900 metros de altitude em relação ao nível do mar.", new DateTime(2000, 3, 3, 3, 3, 3, 0, DateTimeKind.Unspecified), 2.8m, 184, 18, "-27.755497", "-48.675000", "Fácil/Moderado/Difícil", "O Cambirela", true },
                    { 8, 0m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3107), "O Rio do Boi é o rio que forma o Canyon Itaimbezinho, e configura uma  trilha de aventura e  de beleza impressionantes, pois se caminha por dentro da grande garganta do  Canyon entre paredões de até 720m. A origem do nome Rio do Boi surgiu no tempo das tropas que desciam os campos de cima da serra abrindo caminho montanha abaixo, pelas íngremes escarpas da serra geral. Diz a lenda que em uma das' tropeadas' um boi se desgarrou da tropa e caiu no penhasco que culminava no rio de pedras que cortava a montanha. Esta é uma das versões da origem do nome do Rio do Boi.", new DateTime(2000, 6, 6, 6, 6, 6, 0, DateTimeKind.Unspecified), 14m, 209, 18, "-29.201767", "-50.042767", "Moderado/Difícil", "Rio do Boi", true },
                    { 9, 0m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3111), "O Cânion Malacara é outro lindo cânion do Parque Nacional da Serra Geral, com 3,5km de extensão e com largura máxima de 1000m e profundidade de 780m, compõe um ambiente que reúne campos de altitude recortados por pequenos arroios de águas rasas e límpidas que correm em todas as direções possíveis. Guarda consigo ecos de tempos distantes deixados pelos tropeiros que, conduziam gado, porcos e mulas carregadas de produtos serranos, descendo as encostas da serra para negociar e retornando com novos produtos e histórias. Seu nome originou-se de uma pedra em uma das paredes que lembra o formato de um cavalo malacara, que independente da raça, possui uma listra branca na parte frontal de sua cabeça.", new DateTime(2000, 4, 4, 4, 4, 4, 0, DateTimeKind.Unspecified), 5m, 209, 18, "-29.159044", "-49.981984", "Moderado/Difícil", "Canyon Malacara", true },
                    { 10, 624m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3115), "Localizado em Treviso, sul de Santa Catarina, o morro foi apelidado de dois dedos pois para quem olha de baixo é a impressão que tem devido à formação das duas rochas em seu topo.", new DateTime(2000, 2, 2, 2, 2, 2, 0, DateTimeKind.Unspecified), 6m, 275, 18, "-28.511404", "-49.525824", "Difícil", "Trilha Dois Dedos", true },
                    { 3, 847m, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(3086), "Conforme o guia Carlos Reinke, o aceso até o pé do morro pode ser feito por São Bento do Sul, passando pelo bairro Rio Vermelho Estação. Ao chegar ao cume é possível tem uma visão panorâmica da Serra de São Bento, Corupá e Jaraguá do Sul. “A entrada para a trilha é em propriedade particular. É tradicional a existência de uma cruz de madeira no topo do morro, e recentemente recebeu uma cruz em metal”, comenta.", new DateTime(2000, 2, 2, 2, 2, 2, 0, DateTimeKind.Unspecified), 3m, 283, 0, "-26.364364", "-49.258283", "Moderado/Fácil", "Morro da Igreja", true }
                });

            migrationBuilder.InsertData(
                table: "comentarios",
                columns: new[] { "id", "comentario_texto", "data_comentario", "data_criacao", "id_montanha", "id_usuario", "registro_ativo" },
                values: new object[,]
                {
                    { 6, "Ja subi essa montanha foi top de mais", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8085), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8087), 4, 5, true },
                    { 3, "Essa Montanha é Muito Muito Top", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8069), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8072), 1, 2, true },
                    { 5, "Ja subi essa montanha foi top de mais", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8080), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8082), 3, 4, true },
                    { 1, "Essa Montanha é Top", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(7601), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8032), 1, 1, true },
                    { 7, "Nunca subi mas ja tive vontade", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8090), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8093), 5, 5, true },
                    { 4, "Ja subi essa montanha foi top", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8075), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8077), 2, 3, true },
                    { 2, "Essa Montanha é Muito Top", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8060), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8066), 1, 2, true },
                    { 9, "Fui sábado passado, foi legal", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8101), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8103), 4, 4, true },
                    { 8, "Foi legal passar um tempo com a familia", new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8095), new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(8098), 4, 4, true }
                });

            migrationBuilder.InsertData(
                table: "montanhas_categorias",
                columns: new[] { "id", "data_criacao", "id_categoria", "id_montanha", "registro_ativo" },
                values: new object[,]
                {
                    { 2, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5773), 1, 2, true },
                    { 10, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5797), 1, 10, true },
                    { 99, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5795), 4, 9, true },
                    { 9, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5792), 1, 9, true },
                    { 8, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5789), 1, 8, true },
                    { 13, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5806), 1, 13, true },
                    { 7, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5787), 1, 7, true },
                    { 111, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5766), 3, 1, true },
                    { 12, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5803), 1, 12, true },
                    { 1, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5741), 1, 1, true },
                    { 4, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5778), 2, 4, true },
                    { 6, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5784), 1, 6, true },
                    { 5, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5781), 1, 5, true },
                    { 11, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5800), 2, 11, true },
                    { 1111, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5770), 4, 1, true },
                    { 3, new DateTime(2019, 10, 9, 13, 19, 17, 423, DateTimeKind.Local).AddTicks(5775), 2, 3, true }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_UsuarioId",
                table: "AspNetUserRoles",
                column: "UsuarioId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_chats_id_montanha",
                table: "chats",
                column: "id_montanha");

            migrationBuilder.CreateIndex(
                name: "IX_comentarios_id_montanha",
                table: "comentarios",
                column: "id_montanha");

            migrationBuilder.CreateIndex(
                name: "IX_comentarios_id_usuario",
                table: "comentarios",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_guias_id_cidade",
                table: "guias",
                column: "id_cidade");

            migrationBuilder.CreateIndex(
                name: "IX_guias_id_montanha",
                table: "guias",
                column: "id_montanha");

            migrationBuilder.CreateIndex(
                name: "IX_guias_id_usuario",
                table: "guias",
                column: "id_usuario",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_imagens_montanhas_id_montanha",
                table: "imagens_montanhas",
                column: "id_montanha");

            migrationBuilder.CreateIndex(
                name: "IX_locais_id_cidade",
                table: "locais",
                column: "id_cidade");

            migrationBuilder.CreateIndex(
                name: "IX_locais_id_montanha",
                table: "locais",
                column: "id_montanha");

            migrationBuilder.CreateIndex(
                name: "IX_mensagens_id_chat",
                table: "mensagens",
                column: "id_chat");

            migrationBuilder.CreateIndex(
                name: "IX_mensagens_id_guia",
                table: "mensagens",
                column: "id_guia");

            migrationBuilder.CreateIndex(
                name: "IX_mensagens_id_usuario",
                table: "mensagens",
                column: "id_usuario");

            migrationBuilder.CreateIndex(
                name: "IX_montanhas_id_cidade",
                table: "montanhas",
                column: "id_cidade");

            migrationBuilder.CreateIndex(
                name: "IX_montanhas_categorias_id_categoria",
                table: "montanhas_categorias",
                column: "id_categoria");

            migrationBuilder.CreateIndex(
                name: "IX_montanhas_categorias_id_montanha",
                table: "montanhas_categorias",
                column: "id_montanha");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "comentarios");

            migrationBuilder.DropTable(
                name: "imagens_montanhas");

            migrationBuilder.DropTable(
                name: "locais");

            migrationBuilder.DropTable(
                name: "mensagens");

            migrationBuilder.DropTable(
                name: "montanhas_categorias");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "chats");

            migrationBuilder.DropTable(
                name: "guias");

            migrationBuilder.DropTable(
                name: "categorias");

            migrationBuilder.DropTable(
                name: "montanhas");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "cidades");
        }
    }
}
