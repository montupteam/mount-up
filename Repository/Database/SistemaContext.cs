﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Model;
using Model.Identity;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Database
{
    public class SistemaContext : IdentityDbContext<Usuario, Role, int,
                                                    IdentityUserClaim<int>, UsuarioRole, IdentityUserLogin<int>,
                                                    IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
    
        public SistemaContext(DbContextOptions<SistemaContext> options) : base(options){}

        public DbSet<Chat> Chats { get; set; }
        public DbSet<Mensagem> Mensagems { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public DbSet<Montanha> Montanhas { get; set; }
        public DbSet<ImagemMontanha> ImagensMontanhas { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<MontanhaCategoria> MontanhasCategorias { get; set; }
        public DbSet<Guia> Guias { get; set; }
        public DbSet<Local> Locais { get; set; }
        public DbSet<Cidade> Cidades { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                 .SelectMany(t => t.GetForeignKeys())
                 .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Seed();

            base.OnModelCreating(modelBuilder);

            
            modelBuilder.Entity<UsuarioRole>(usuarioRole =>
                {
                    usuarioRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                    //Feito para que o Indetity entenda o relacionamento NpN de forma correta
                    usuarioRole.HasOne(ur => ur.Role)
                        .WithMany(r => r.UsuarioRoles)
                        .HasForeignKey(ur => ur.RoleId)
                        .IsRequired();

                    usuarioRole.HasOne(ur => ur.Role)
                        .WithMany(r => r.UsuarioRoles)
                        .HasForeignKey(ur => ur.UserId)
                        .IsRequired();

                    //O relacionamento de NpN, na tabela UsuarioRole,
                    //ele vai ser realmente dado por meio das chaves estrangeiras dentro
                    //de UsuarioRole, que vem de Role e de Usuario. E no final fazendo com que 
                    //ambas sejam de preenchimento obrigatório 
                }
            );            
        }
    }
}
