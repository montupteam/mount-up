﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class LocalRepository : ILocalRepository
    {
        private readonly SistemaContext _context;

        public LocalRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(Local local)
        {
            local.RegistroAtivo = true;
            local.DataCriacao = DateTime.Now;
            _context.Locais.Add(local);
            _context.SaveChanges();
            return local.Id;
        }

        public bool Delete(int id)
        {
            var local = _context.Locais.FirstOrDefault(x => x.Id == id);
            if (local == null)
                return false;
            local.RegistroAtivo = false;
            return _context.SaveChanges() == 1;
        }

        public Local ObterPeloId(int id)
        {
            return _context.Locais
                .Include(x => x.Cidade)
                .Include(x => x.Montanha)
                .FirstOrDefault(x => x.Id == id && x.RegistroAtivo);
        }

        public List<Local> ObterTodos()
        {
            return _context.Locais
                .Include(x => x.Cidade)
                .Include(x => x.Montanha)
                .Where(x => x.RegistroAtivo)
                .ToList();
        }

        public bool Update(Local local)
        {
            local.RegistroAtivo = true;
            local.DataCriacao = DateTime.Now;
            _context.Locais.Update(local);
            return _context.SaveChanges() == 1;
        }
    }
}
