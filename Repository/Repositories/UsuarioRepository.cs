﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly SistemaContext _context;

        public UsuarioRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(Usuario usuario)
        {
            usuario.RegistroAtivo = true;
            usuario.DataCriacao = DateTime.Now;
            _context.Add(usuario);
            _context.SaveChanges();
            return usuario.Id;
        }

        public bool Delete(int id)
        {
            var usuario = _context.Usuarios.FirstOrDefault(x => x.Id == id);
            if (usuario == null)
            {
                return false;
            }
            usuario.RegistroAtivo = false;
            return _context.SaveChanges() == 1;
        }

        public Usuario ObterPeloId(int id)
        {
            return _context.Usuarios
                .Include(x => x.Guia)
                .FirstOrDefault(x => x.RegistroAtivo && x.Id == id);
        }

        public List<Usuario> ObterTodos()
        {
            return _context.Usuarios.Where(x => x.RegistroAtivo).ToList();
        }

        public bool Update(Usuario usuario)
        {
            usuario.RegistroAtivo = true;
            usuario.DataCriacao = DateTime.Now;
            _context.Update(usuario);
            return _context.SaveChanges() == 1;
        }
    }
}
