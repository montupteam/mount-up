﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.ViewModel;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repositories
{
    public class ImagemMontanhaRepository : IImagemMontanhaRepository
    {
        private readonly SistemaContext _context;
        public ImagemMontanhaRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(ImagemMontanha imagemMontanha)
        {
            imagemMontanha.DataCriacao = DateTime.Now;
            imagemMontanha.RegistroAtivo = true;
            _context.Add(imagemMontanha);
            _context.SaveChanges();
            return imagemMontanha.Id;
        }

        public bool Delete(int id)
        {
            var imagemMontanha = _context.ImagensMontanhas.FirstOrDefault(x => x.Id == id);
            if (imagemMontanha == null)
                return false;

            imagemMontanha.RegistroAtivo = false;
            return _context.SaveChanges() == 1;
        }

        public ImagemMontanha ObterPeloId(int id)
        {
            return _context.ImagensMontanhas
                .Include(x => x.Montanha)
                .FirstOrDefault(x => x.Id == id && x.RegistroAtivo);
        }

        public ImagemMontanha ObterPeloIdMontanha(int idMontanha)
        {
            return _context.ImagensMontanhas
                .Include(x => x.Montanha)
                .FirstOrDefault(x => x.IdMontanha == idMontanha && x.RegistroAtivo);
        }

        public List<ImagemMontanha> ObterTodosPeloIdMontanha(int idMontanha)
        {
            return _context.ImagensMontanhas
                .Include(x => x.Montanha)
                .Where(x => x.IdMontanha == idMontanha && x.RegistroAtivo)
                .ToList();
        }

        public bool Update(ImagemMontanha imagemMontanha)
        {
            imagemMontanha.RegistroAtivo = true;
            _context.ImagensMontanhas.Update(imagemMontanha);
            return _context.SaveChanges() == 1;
        }
    }
}
