﻿using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class GenericRepository : IBaseRepository, IBaseReadRepository
    {
        private readonly SistemaContext _context;
        public GenericRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add<T>(T entity) where T : Base
        {
            entity.DataCriacao = DateTime.Now;
            entity.RegistroAtivo = true;
            _context.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        public bool Delete<T>(int id) where T : Base
        {
            T entidade = _context.Set<T>().FirstOrDefault(x => x.Id == id);
            if (entidade == null)
            {
                throw new ArgumentException("Id não encontrado na tabela " + entidade.GetType().Name);
            }
            entidade.RegistroAtivo = false;
            _context.SaveChanges();
            return true;
        }

        public T ObterPeloId<T>(int id) where T : Base
        {
            return _context.Set<T>().FirstOrDefault(x => x.RegistroAtivo && x.Id == id);
        }

        public List<T> ObterTodos<T>() where T : Base
        {
            return _context.Set<T>().Where(x => x.RegistroAtivo).ToList();
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }

        public bool Update<T>(T entity) where T : Base
        {
            entity.DataCriacao = DateTime.Now;
            entity.RegistroAtivo = true;
            _context.Set<T>().Update(entity);
            return _context.SaveChanges() == 1;
        }
    }
}
