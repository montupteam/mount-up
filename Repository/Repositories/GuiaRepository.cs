﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public class GuiaRepository : IGuiaRepository
    {
        private readonly SistemaContext _context;
        public GuiaRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(Guia guia)
        {
            guia.DataCriacao = DateTime.Now;
            guia.RegistroAtivo = true;
            _context.Add(guia);
            _context.SaveChanges();
            return guia.Id;
        }

        public bool Delete(int id)
        {
            var guia = _context.Guias.FirstOrDefault(x => x.Id == id);
            if (guia == null)
                return false;

            guia.RegistroAtivo = false;
            return _context.SaveChanges() == 1;
        }

        public Guia ObterPeloId(int id)
        {
            return _context.Guias
                .Include(x => x.Montanha)
                .Include(x => x.Cidade)
                .Include(x => x.Usuario)
                .FirstOrDefault(x => x.Id == id && x.RegistroAtivo);
        }

        public Guia ObterPeloIdUsuario(int idUsuario)
        {
            return _context.Guias
                .Include(x => x.Usuario)
                .FirstOrDefault(x => x.IdUsuario == idUsuario && x.RegistroAtivo);
        }

        public List<Guia> ObterTodos(string busca, int idCidade, int idMontanha)
        {
            var query = _context.Guias
                .Include(x => x.Montanha) 
                .Include(x => x.Cidade)
                .Include(x => x.Usuario)
                .Where(x => x.RegistroAtivo)
                .AsQueryable();

            if (!string.IsNullOrEmpty(busca))
                query = query.Where(x => x.Usuario.UserName.Contains(busca));

            if (idCidade != 0)
                query = query.Where(x => x.Montanha.IdCidade == idCidade);

            if (idMontanha != 0)
                query = query.Where(x => x.IdMontanha == idMontanha);
                            
            return query.ToList();
        }

        public bool Update(Guia guia)
        {
            guia.RegistroAtivo = true;
            guia.DataCriacao = DateTime.Now;
            _context.Guias.Update(guia);
            return _context.SaveChanges() == 1;
        }
    }
}
