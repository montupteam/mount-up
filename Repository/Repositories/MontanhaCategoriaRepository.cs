﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.ViewModel;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repositories
{
    public class MontanhaCategoriaRepository : IMontanhaCategoriaRepository
    {
        private readonly SistemaContext _context;
        public MontanhaCategoriaRepository(SistemaContext context)
        {
            _context = context;
        }

        public MontanhaCategoria ObterPeloId(int id)
        {
            return _context.MontanhasCategorias.FirstOrDefault(x => x.Id == id && x.RegistroAtivo);
        }

        public List<MontanhaCategoria> ObterTodosPeloIdMontanha(int idMontanha)
        {
            return _context.MontanhasCategorias
                .Include(x => x.Categoria)
                .Where(x => x.IdMontanha == idMontanha)
                .ToList();
        }

        public int Relacionar(MontanhaCategoria montanhaCategoria)
        {
            montanhaCategoria.DataCriacao = DateTime.Now;
            montanhaCategoria.RegistroAtivo = true;
            _context.Add(montanhaCategoria);
            _context.SaveChanges();
            return montanhaCategoria.Id;
        }

        public List<int> RelacionarVarios(MontanhaCategoriaViewModel montanhaCategoriaViewModel)
        {
            var retList = new List<int>();
            int idMontanha = montanhaCategoriaViewModel.IdMontanha;
            foreach (var idCategoria in montanhaCategoriaViewModel.Categorias)
            {
                var montanhaCategoria = new MontanhaCategoria()
                {
                    IdCategoria = idCategoria,
                    IdMontanha = idMontanha
                };
                montanhaCategoriaViewModel.RegistroAtivo = true;
                montanhaCategoriaViewModel.DataCriacao = DateTime.Now;
                montanhaCategoria.DataCriacao = DateTime.Now;
                montanhaCategoria.RegistroAtivo = true;
                _context.MontanhasCategorias.Add(montanhaCategoria);
                _context.SaveChanges();
                var teste = montanhaCategoria.Id;
                retList.Add(teste);
            }
            return retList;
        }
    }
}
