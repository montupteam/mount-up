﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repositories
{
    public class MensagemRepository : IMensagemRepository
    {
        private readonly SistemaContext _context;
        public MensagemRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(Mensagem mensagem)
        {
            mensagem.DataCriacao = DateTime.Now;
            mensagem.RegistroAtivo = true;
            _context.Mensagems.Add(mensagem);
            _context.SaveChanges();
            return mensagem.Id;
        }

        public bool Delete(int id)
        {
            var mensagem = _context.Mensagems.FirstOrDefault(x => x.Id == id);
            if (mensagem == null)
                return false;

            mensagem.RegistroAtivo = false;
            return _context.SaveChanges() == 1;
        }

        public List<Mensagem> ObterTodosPeloIdDoChat(int idChat)
        {
            return _context.Mensagems
                .Include(x => x.Usuario)
                .Include(x => x.Guia)
                .Where(x => x.IdChat == idChat && x.RegistroAtivo == true).ToList();
        }

        public bool Update(Mensagem mensagem)
        {
            mensagem.RegistroAtivo = true;
            _context.Mensagems.Update(mensagem);
            return _context.SaveChanges() == 1;
        }
    }
}
