﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repositories
{
    public class ChatRepository : IChatRepository
    {
        private readonly SistemaContext _context;

        public ChatRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(Chat chat)
        {
            chat.DataCriacao = DateTime.Now;
            chat.RegistroAtivo = true;
            _context.Add(chat);
            _context.SaveChanges();
            return chat.Id;
        }

        public bool Delete(int id)
        {
            var chat = _context.Chats.FirstOrDefault(x => x.Id == id);
            if (chat == null)
                return false;

            chat.RegistroAtivo = false;
            return _context.SaveChanges() == 1;

        }

        public Chat ObterPeloId(int id)
        {
            return _context.Chats
                .Include(x => x.Montanha)
                .FirstOrDefault(x => x.Id == id && x.RegistroAtivo == true);
        }

        public List<Chat> ObterTodos()
        {
            return _context.Chats
                .Include(x => x.Montanha)
                .Where(x => x.RegistroAtivo)
                .ToList();
        }
    }
}
