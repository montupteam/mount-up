﻿ using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repositories
{
    public class MontanhaRepository : IMontanhaRepository
    {
        private readonly SistemaContext _context;

        public MontanhaRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(Montanha montanha)
        {
            montanha.RegistroAtivo = true;
            montanha.DataCriacao = DateTime.Now;
            _context.Add(montanha);
            _context.SaveChanges();
            return montanha.Id;
        }

        public bool Delete(int id)
        {
            var montanha = _context.Montanhas.FirstOrDefault(x => x.Id == id);
            if (montanha == null)
            {
                return false;
            }
            montanha.RegistroAtivo = false;
            return _context.SaveChanges() == 1;
        }

        public Montanha ObterPeloId(int id)
        {
            return _context.Montanhas.FirstOrDefault(x => x.RegistroAtivo && x.Id == id);
        }

        public List<Montanha> ObterTodos(int idCidade, int idCategoria, int menorDuracao, int maiorDuracao, string busca)
        {
            var query = _context.Montanhas
                .Include(c => c.Cidade)
                .Include(x => x.MontanhasCategoria)
                .ThenInclude(y => y.Categoria)
                .Where(x => x.RegistroAtivo).OrderBy(x => x.Nome)
                .AsQueryable();

            if (!string.IsNullOrEmpty(busca))
                query = query.Where(x => x.Nome.Contains(busca));

            if (menorDuracao != 0 && maiorDuracao != 0)
                query = query.Where(x => x.Duracao.Hour >= menorDuracao && x.Duracao.Hour <= maiorDuracao);

            if (idCategoria != -1)
                query = query.Where(x => x.MontanhasCategoria.Any(mc => mc.IdCategoria == idCategoria));

            if (idCidade != -1)
                query = query.Where(c => c.IdCidade == idCidade);

            return query.ToList();
        }

        public List<Montanha> ObterTodasImagens(int idMontanha)
        {
            var query = _context.Montanhas
                .Include(x => x.ImagensMontanhas)
                .Where(x => x.RegistroAtivo)
                .AsQueryable();

            if (idMontanha != -1)
                query = query.Where(x => x.ImagensMontanhas.Any(im => im.IdMontanha == idMontanha));

            return query.ToList();
        }

        public bool Update(Montanha montanha)
        {
            montanha.RegistroAtivo = true;
            montanha.DataCriacao = DateTime.Now;
            _context.Update(montanha);
            return _context.SaveChanges() == 1;
        }
    }
}
