﻿using Microsoft.EntityFrameworkCore;
using Model;
using Repository.Database;
using Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Repositories
{
    public class ComentarioRepository : IComentarioRepository
    {

        public readonly SistemaContext _context;

        public ComentarioRepository(SistemaContext context)
        {
            _context = context;
        }

        public int Add(Comentario comentario)
        {
            comentario.RegistroAtivo = true;
            comentario.DataComentario = DateTime.Now;
            comentario.DataCriacao = DateTime.Now;
            _context.Comentarios.Add(comentario);
            _context.SaveChanges();
            return comentario.Id;
        }

        public bool Update(Comentario comentario)
        {
            comentario.RegistroAtivo = true;
            comentario.DataComentario = DateTime.Now;
            _context.Comentarios.Update(comentario);
            return _context.SaveChanges() == 1;
        }

        public bool Delete(int id)
        {
            var comentario = _context.Comentarios.FirstOrDefault(x => x.Id == id);
            if (comentario == null)
                return false;
            comentario.RegistroAtivo = false;
            return _context.SaveChanges() == 1;
        }

        public List<Comentario> ObterTodos()
        {
            return _context.Comentarios
                .Include(x => x.Usuario)
                .Include(x => x.Montanha)
                .OrderByDescending(x => x.DataComentario)
                .Where(x => x.RegistroAtivo).ToList();
        }

        public Comentario ObterPeloId(int id)
        {
            return _context.Comentarios
                .Include(x => x.Usuario) 
                .Include(x => x.Montanha)
                .FirstOrDefault(x => x.Id == id && x.RegistroAtivo);
        }
    }
}
