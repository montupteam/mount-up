﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("chats")]
    public class Chat : Base
    {
        [ForeignKey("IdMontanha")]
        public Montanha Montanha { get; set; }
        [Column("id_montanha", TypeName = "INT")]
        public int IdMontanha { get; set; }

    }
}
