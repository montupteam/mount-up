﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("cidades")]
    public class Cidade : Base
    {
        [Column("nome", TypeName = "VARCHAR(100)")]
        public string Nome { get; set; }
    }
}
