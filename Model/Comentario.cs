﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("comentarios")]
    public class Comentario : Base
    {
        [ForeignKey("IdUsuario")]
        public Usuario Usuario { get; set; }

        [Column("id_usuario", TypeName = "INT")]
        public int IdUsuario { get; set; }

        [ForeignKey("IdMontanha")]
        public Montanha Montanha { get; set; }

        [Column("id_montanha", TypeName = "INT")]
        public int IdMontanha { get; set; }

        [Column("comentario_texto", TypeName = "TEXT")]
        public string ComentarioTexto { get; set; }

        [Column("data_comentario", TypeName = "DATETIME")]
        public DateTime DataComentario { get; set; }
    }
}
