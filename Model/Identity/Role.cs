﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.Identity
{
    [Table("roles")]
    public class Role : IdentityRole<int>
    {
        public List<UsuarioRole> UsuarioRoles { get; set; }
    }
}
