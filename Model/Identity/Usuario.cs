﻿using Microsoft.AspNetCore.Identity;
using Model.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("usuarios")]
    public class Usuario :  IdentityUser<int>
    {

        [InverseProperty("Usuario")]
        public Guia Guia { get; set; }

        [Column("nome", TypeName = "VARCHAR(100)")]
        public string Nome { get; set; }

        [Column("sobrenome", TypeName = "VARCHAR(100)")]
        public string Sobrenome { get; set; }

        [Column("cpf", TypeName = "VARCHAR(100)")]
        public string Cpf { get; set; }

        [Column("data_nascimento", TypeName = "DATETIME")]
        public DateTime? DataNascimento { get; set; }

        [Column("email", TypeName = "VARCHAR(100)")]
        public override string Email { get; set; }

        [Column("confirmar_email", TypeName = "BIT")]
        public bool ConfirmarEmail { get; set; }

        [Column("senha", TypeName = "VARCHAR(350)")]
        public string Password { get; set; }

        [Column("registro_ativo", TypeName = "BIT")]
        public bool RegistroAtivo { get; set; }

        [Column("data_criacao", TypeName = "DATETIME")]
        public DateTime DataCriacao { get; set; }

        [Column("imagem_usuario", TypeName = "VARCHAR(100)")]
        public string ImagemUsuario { get; set; }

        [Column("imagem_hash", TypeName = "VARCHAR(100)")]
        public string ImagemHash { get; set; }

        [Column("usuarios_roles")]
        public List<UsuarioRole> UsuarioRoles { get; set; }

    }
}
