﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.Identity
{
    [Table("usuarios_roles")]
    public class UsuarioRole : IdentityUserRole<int>
    {
        public Usuario Usuario { get; set; }

        public Role Role { get; set; }
    }
}