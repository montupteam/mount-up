﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("categorias")]
    public class Categoria : Base
    {
        [Column("nome", TypeName = "VARCHAR(100)")]
        public string Nome { get; set; }

     
    }
}
