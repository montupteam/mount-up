﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModel
{
    public class GuiaViewModel
    {
        public int IdUsuario { get; set; }
        public int IdMontanha { get; set; }
        public int IdCidade { get; set; }
        public string Instagram { get; set; }
        public string Telefone { get; set; }
        public string Ocupacao { get; set; }
        public string CodigoPostal { get; set; }
        public string CertificadoHash { get; set; }
        public bool TemCertificado { get; set; }
        public string ConhecimentoMontanha { get; set; }
        public string DescricaoGuia { get; set; }
        public string DescricaoExperiencia { get; set; }
        public IFormFile Certificado { get; set; }
    }
}
