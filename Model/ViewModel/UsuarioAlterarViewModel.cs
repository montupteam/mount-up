﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModel
{
    public class UsuarioAlterarViewModel
    {
        public string UserName { get; set; }
        public string Sobrenome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public DateTime? DataNascimento { get; set; }
        public bool RegistroAtivo { get; set; }
        public DateTime DataCriacao { get; set; }
    }
}
