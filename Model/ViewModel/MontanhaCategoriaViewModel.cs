﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.ViewModel
{
    public class MontanhaCategoriaViewModel : Base
    {
        public int IdMontanha { get; set; }
        public List<int> Categorias { get; set; }
    }
}
