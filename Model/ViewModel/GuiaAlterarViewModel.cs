﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModel
{
    public class GuiaAlterarViewModel
    {
        public int Id { get; set; }
        public int IdMontanha { get; set; }
        public int IdCidade { get; set; }
        public string Instagram { get; set; }
        public string Telefone { get; set; }
        public string Ocupacao { get; set; }
        public string CodigoPostal { get; set; }
        public bool TemCertificado { get; set; }
        public string ConhecimentoMontanha { get; set; }
        public string DescricaoGuia { get; set; }
        public string DescricaoExperiencia { get; set; }



        /*	"Id": 2,
	"IdMontanha": 1,
	"IdCidade": 1,
	"Instragram": "@algumacoisa123",
	"Telefone": "33363726",
	"Ocupacao": "Teste",
	"CodigoPostal": "87042800",
	"TemCertificado": true,
	"ConhecimentoMontanha": "alguma coisa",
	"DescricaoGuia": "Mito",
	"DescricaoExperiencia": "Santo deus"
        */
    }
}
