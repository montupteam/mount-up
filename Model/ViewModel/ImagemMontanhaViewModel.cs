﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Model.ViewModel
{
    public class ImagemMontanhaViewModel: Base
    {
        public int MontanhaID { get; set; }

        public IFormFile Imagem { get; set; }
    }
}
