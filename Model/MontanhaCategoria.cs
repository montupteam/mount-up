﻿using Model.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("montanhas_categorias")]
    public class MontanhaCategoria : Base
    {
        [ForeignKey("IdCategoria")]
        public virtual Categoria Categoria { get; set; }

        [Column("id_categoria", TypeName = "INT")]
        public int IdCategoria { get; set; }

        [ForeignKey("IdMontanha")]
        public virtual Montanha Montanha { get; set; }

        [Column("id_montanha", TypeName = "INT")]
        public int IdMontanha { get; set; }
    }
}
