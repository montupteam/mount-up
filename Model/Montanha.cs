﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("montanhas")]
    public class Montanha : Base
    {
        [Column("nome", TypeName = "VARCHAR(100)")]
        public string Nome { get; set; }

        [Column("altitude", TypeName = "DECIMAL(8,2)")]
        public decimal Altitude { get; set; }

        [Column("extensao", TypeName = "DECIMAL(8,2)")]
        public decimal Extensao { get; set; }

        [Column("duracao", TypeName = "DATETIME")]
        public DateTime Duracao { get; set; }

        [Column("nivel", TypeName = "VARCHAR(100)")]
        public string Nivel { get; set; }

        [Column("idade_minima", TypeName = "INT")]
        public int IdadeMinima { get; set; }

        [Column("descricao", TypeName = "TEXT")]
        public string Descricao { get; set; }

        [Column("latitude", TypeName = "VARCHAR(100)")]
        public string Latitude { get; set; }

        [Column("longitude", TypeName = "VARCHAR(100)")]
        public string Longitude { get; set; }

        [Column("id_cidade", TypeName = "INT")]
        public int IdCidade { get; set; }

        [ForeignKey("IdCidade")]
        public Cidade Cidade { get; set; }

        public List<MontanhaCategoria> MontanhasCategoria { get; set; }

        public List<ImagemMontanha> ImagensMontanhas { get; set; }
    }
}
