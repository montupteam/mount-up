﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("imagens_montanhas")]
    public class ImagemMontanha : Base
    {
        [ForeignKey("IdMontanha")]
        public Montanha Montanha { get; set; }

        [Column("id_montanha", TypeName = "INT")]
        public int IdMontanha { get; set; }

        [Column("url_imagem", TypeName = "VARCHAR(100)")]
        public string UrlImagem { get; set; }

        [Column("url_imagem_hash", TypeName = "VARCHAR(100)")]
        public string UrlImagemHash { get; set; }
    }
}
