﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("mensagens")]
    public class Mensagem : Base
    {
        [ForeignKey("IdChat")]
        public virtual Chat Chat { get; set; }

        [Column("id_chat", TypeName = "INT")]
        public int IdChat { get; set; }

        [ForeignKey("IdUsuario")]
        public virtual Usuario Usuario { get; set; }

        [Column("id_usuario", TypeName = "INT")]
        public int? IdUsuario { get; set; }

        [ForeignKey("IdGuia")]
        public virtual Guia Guia { get; set; }
        [Column("id_guia", TypeName = "INT")]
        public int? IdGuia { get; set; }

        [Column("texto", TypeName = "TEXT")]
        public string Texto { get; set; }

    }
}
