﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("guias")]
    public class Guia : Base
    {
        [ForeignKey("IdUsuario")]
        public Usuario Usuario { get; set; }

        [Column("id_usuario", TypeName = "INT")]
        public int IdUsuario { get; set; }

        [ForeignKey("IdMontanha")]
        public Montanha Montanha { get; set; }

        [Column("id_montanha", TypeName = "INT")]
        public int IdMontanha { get; set; }

        [ForeignKey("IdCidade")]
        public Cidade Cidade { get; set; }

        [Column("id_cidade", TypeName = "INT")]
        public int IdCidade { get; set; }

        [Column("instagram", TypeName = "VARCHAR(100)")]
        public string Instagram { get; set; }

        [Column("telefone", TypeName = "VARCHAR(100)")]
        public string Telefone { get; set; }

        [Column("ocupacao", TypeName = "VARCHAR(100)")]
        public string Ocupacao { get; set; }

        [Column("codigo_postal", TypeName = "VARCHAR(100)")]
        public string CodigoPostal { get; set; }

        [Column("certificado", TypeName = "VARCHAR(100)")]
        public string Certificado { get; set; }

        [Column("certificado_hash", TypeName = "VARCHAR(100)")]
        public string CertificadoHash { get; set; }

        [Column("tem_certificado", TypeName = "BIT")]
        public bool TemCertificado { get; set; }

        [Column("descricao_guia", TypeName = "TEXT")]
        public string DescricaoGuia { get; set; }

        [Column("descricao_experiencia", TypeName = "TEXT")]
        public string DescricaoExperiencia { get; set; }
    }
}
