﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model
{
    [Table("locais")]
    public class Local : Base
    {
        [ForeignKey("IdCidade")]
        public Cidade Cidade { get; set; }
        [Column("id_cidade", TypeName = "INT")]
        public int IdCidade { get; set; }

        [ForeignKey("IdMontanha")]
        public Montanha Montanha { get; set; }
        [Column("id_montanha", TypeName = "INT")]
        public int IdMontanha { get; set; }

        [Column("nome", TypeName = "VARCHAR(100)")]
        public string Nome { get; set; }

        [Column("descricao", TypeName = "TEXT")]
        public string Descricao { get; set; }
    }
}
